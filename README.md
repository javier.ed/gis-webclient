# Bululú Webclient

[![pipeline status](https://gitlab.com/bululu/bululu-webclient/badges/master/pipeline.svg)](https://gitlab.com/bululu/bululu-webclient/commits/master)

## Version

### 0.3-snapshot

## Requirements

* Node.js 12.16+
* Yarn 1.22

## Installation instructions

### Basic configuration

```bash
cp .env.local.example .env.local
```

### Install dependencies

```bash
yarn install
```

### Development deployment

```bash
yarn dev
```
