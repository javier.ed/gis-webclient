import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Typography, Button } from '@material-ui/core';
import { Formik, FormikValues, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { CurrentUserMarkerQueryComponent, Marker } from '../../../graphql/queries/CurrentUserMarkerQuery.graphql';
import { AppContext } from '../../_app';
import BaseLayout from '../../../layouts/base-layout';
import { useDeleteMarkerMutationMutation } from '../../../graphql/mutations/DeleteMarkerMutation.graphql';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';

export default () => {
  const { showSnackbar, session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [marker, setMarker] = useState<Marker | null | undefined>();

  session.requireAuthentication(router);

  const [deleteMarkerMutation] = useDeleteMarkerMutationMutation();

  const title = () => {
    if (marker) {
      return `${i18n.t('yourMarkers')} > ${marker.name} > ${i18n.t('delete')}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (marker) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourMarkers'),
              href: '/your-markers',
              className: 'app-bar-title',
            },
            {
              label: marker.name as string,
              href: '/your-markers/[id]',
              as: `/your-markers/${marker.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('delete'),
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserMarkerQueryComponent variables={{ id: router.query.id as string }}>
        {(result) => {
          setMarker(result.data?.currentUser?.marker);
          if (marker) {
            return (
              <Formik
                initialValues={{ name: '', confirm: false }}
                validate={(values) => {
                  const errors: Partial<FormikValues> = {};
                  if (values.name !== marker.name) {
                    errors.name = i18n.t('isInvalid');
                  }
                  return errors;
                }}
                onSubmit={(_values, { setSubmitting }) => {
                  deleteMarkerMutation({ variables: { id: marker.id } })
                    .then((r) => {
                      const deleteMarker = r.data?.deleteMarker;

                      if (deleteMarker?.success) {
                        showSnackbar(deleteMarker.message || '', 'success');
                        router.push('/', '/');
                      } else {
                        showSnackbar(deleteMarker?.message || '', 'error');
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                      setSubmitting(false);
                    });
                }}
              >
                {({ submitForm, isSubmitting }) => (
                  <Form autoComplete="off">
                    <Typography>{i18n.t('enterTheNameOfTheMarkerYouWantToDelete')}</Typography>
                    <Field
                      component={TextField}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="name"
                      label={i18n.t('name')}
                    />
                    <br />
                    <br />
                    <Button disabled={isSubmitting} onClick={submitForm} variant="contained" color="default" fullWidth>
                      {i18n.t('delete')}
                    </Button>
                  </Form>
                )}
              </Formik>
            );
          }
          return <></>;
        }}
      </CurrentUserMarkerQueryComponent>
    </BaseLayout>
  );
};
