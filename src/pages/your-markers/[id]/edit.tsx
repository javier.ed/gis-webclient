import { useContext, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { FormikValues } from 'formik';
import BaseLayout from '../../../layouts/base-layout';
import { AppContext } from '../../_app';
import { useUpdateMarkerMutationMutation } from '../../../graphql/mutations/UpdateMarkerMutation.graphql';
import {
  EditCurrentUserMarkerQueryComponent,
  Marker,
} from '../../../graphql/queries/EditCurrentUserMarkerQuery.graphql';
import MarkerForm from '../../../components/marker-form';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';
import { dateParse, fixDateLocale } from '../../../utils/date-helpers';
import { createApolloClient } from '../../../utils/apollo-client';

export default () => {
  const { showSnackbar, session, i18n, locale } = useContext(AppContext);
  const router = useRouter();
  const [marker, setMarker] = useState<Marker | null | undefined>();
  const [selectedLocale, setSelectedLocale] = useState(locale);
  const [apolloClient, setApolloClient] = useState(createApolloClient());

  session.requireAuthentication(router);

  useEffect(() => {
    setApolloClient(createApolloClient({ 'Accept-Language': selectedLocale }));
  }, [selectedLocale]);

  const [updateMarkerMutation] = useUpdateMarkerMutationMutation();

  const title = () => {
    if (marker) {
      return `${i18n.t('yourMarkers')} > ${marker.name} > ${i18n.t('edit')}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (marker) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourMarkers'),
              href: '/your-markers',
              className: 'app-bar-title',
            },
            {
              label: marker.name as string,
              href: '/your-markers/[id]',
              as: `/your-markers/${marker.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('edit'),
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <EditCurrentUserMarkerQueryComponent
        variables={{ id: router.query.id as string }}
        skip={typeof router.query.id === 'undefined' || router.query.id == null}
        fetchPolicy="network-only"
        client={apolloClient}
      >
        {(result) => {
          setMarker(result.data?.marker);
          const layer = marker?.layer;
          if (marker && layer) {
            return (
              <MarkerForm
                editing
                layer={{
                  id: layer.id,
                  name: layer.name,
                  timeInterval: layer.timeInterval,
                  startAtCurrentTime: layer.startAtCurrentTime,
                  suggestions: layer.suggestions,
                  fields: layer.fields?.nodes?.map((f) => ({
                    id: f?.id,
                    fieldType: f?.fieldType,
                    name: f?.name || '',
                    description: f?.description,
                    required: f?.required,
                    editable: f?.editable,
                    unique: f?.unique,
                    options: f?.options?.nodes?.map((o) => ({ id: o?.id, name: o?.name })),
                  })),
                }}
                initialValues={{
                  locale: selectedLocale,
                  name: marker.name || '',
                  description: marker.description || '',
                  avatarUrl: marker.avatarUrl || undefined,
                  startAt: marker.startAt,
                  finishAt: marker.finishAt,
                  geocodingPoint: {
                    latitude: marker.geocodingPoint?.latitude as number,
                    longitude: marker.geocodingPoint?.longitude as number,
                    address: marker.geocodingPoint?.address,
                  },
                  values: layer.fields?.nodes?.map((f) => {
                    const value = marker.values?.nodes?.find((v) => v?.layerField?.id === f?.id);
                    return {
                      layerFieldId: f?.id as string,
                      stringValue: value?.stringValue || undefined,
                      textValue: value?.textValue || undefined,
                      integerValue: value?.integerValue || undefined,
                      floatValue: value?.integerValue || undefined,
                      booleanValue: value?.booleanValue || undefined,
                      dateValue: value?.dateValue ? dateParse(value.dateValue) : null,
                      timeValue: value?.timeValue,
                      datetimeValue: value?.datetimeValue,
                      layerFieldOptionId: value?.layerFieldOption?.id || '0',
                    };
                  }),
                }}
                onLocaleChange={setSelectedLocale}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  updateMarkerMutation({
                    variables: {
                      id: marker.id,
                      attributes: {
                        locale: values.locale,
                        layerId: layer.id || '',
                        name: values.name,
                        description: values.description,
                        avatar: values.avatar,
                        removeAvatar: values.removeAvatar,
                        geocodingPoint: {
                          latitude: values.geocodingPoint?.latitude as number,
                          longitude: values.geocodingPoint?.longitude as number,
                        },
                        startAt: layer.timeInterval === 'disabled' || layer.startAtCurrentTime ? null : values.startAt,
                        finishAt: layer.timeInterval === 'any' ? values.finishAt : null,
                        values: values.values?.map((v) => ({
                          layerFieldId: v.layerFieldId,
                          stringValue: v.stringValue,
                          textValue: v.textValue,
                          integerValue: v.integerValue,
                          floatValue: v.floatValue,
                          booleanValue: v.booleanValue,
                          dateValue: v.dateValue ? fixDateLocale(v.dateValue) : undefined,
                          timeValue: v.timeValue,
                          datetimeValue: v.datetimeValue,
                          layerFieldOptionId: v.layerFieldOptionId !== '0' ? v.layerFieldOptionId : undefined,
                        })),
                      },
                    },
                  })
                    .then((r) => {
                      const updateMarker = r.data?.updateMarker;

                      if (updateMarker?.success) {
                        showSnackbar(updateMarker.message || '', 'success');
                        router.push({
                          pathname: '/',
                          query: { layerId: layer?.id, markerId: updateMarker.marker?.id },
                        });
                      } else {
                        showSnackbar(updateMarker?.message || '', 'error');
                        const errors: Partial<FormikValues> = { data: [{ values: [] }] };
                        updateMarker?.errors?.forEach((error: { path: string[]; message: string }) => {
                          switch (error.path[1]) {
                            case 'data':
                              if (error.path.length === 6 && !isNaN(Number(error.path[4]))) {
                                if (!errors.data[0].values[Number(error.path[4])]) {
                                  errors.data[0].values[Number(error.path[4])] = {};
                                }
                                errors.data[0].values[Number(error.path[4])][error.path[5]] = error.message;
                              }
                              break;
                            default:
                              errors[error.path[1]] = error.message;
                              break;
                          }
                        });
                        setErrors(errors);
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                      setSubmitting(false);
                    });
                }}
              />
            );
          }
          return <></>;
        }}
      </EditCurrentUserMarkerQueryComponent>
    </BaseLayout>
  );
};
