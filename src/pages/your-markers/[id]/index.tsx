import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Card, CardActionArea, CardContent, Typography } from '@material-ui/core';
import { CurrentUserMarkerQueryComponent, Marker } from '../../../graphql/queries/CurrentUserMarkerQuery.graphql';
import { AppContext } from '../../_app';
import BaseLayout from '../../../layouts/base-layout';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [marker, setMarker] = useState<Marker | null | undefined>();

  session.requireAuthentication(router);

  const title = () => {
    if (marker) {
      return `${i18n.t('yourMarkers')} > ${marker.name}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (marker) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            { label: i18n.t('yourMarkers'), href: '/your-markers', className: 'app-bar-title' },
            { label: marker.name as string, className: 'app-bar-title' },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserMarkerQueryComponent variables={{ id: router.query.id as string }}>
        {(result) => {
          setMarker(result.data?.currentUser?.marker);
          if (marker) {
            return (
              <>
                <Link href={`/?markerId=${marker.id}`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('view')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
                <Link href="/your-markers/[id]/edit" as={`/your-markers/${marker.id}/edit`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('edit')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
                <Link href="/your-markers/[id]/delete" as={`/your-markers/${marker.id}/delete`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('delete')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
              </>
            );
          }
          return <></>;
        }}
      </CurrentUserMarkerQueryComponent>
    </BaseLayout>
  );
};
