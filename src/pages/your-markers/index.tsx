import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Card, CardActionArea, CardContent, Typography, CircularProgress, Grid } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';
import Link from 'next/link';
import { CurrentUserMarkersQueryComponent, Marker } from '../../graphql/queries/CurrentUserMarkersQuery.graphql';
import { AppContext } from '../_app';
import BaseLayout from '../../layouts/base-layout';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [searchQuery, setSearchQuery] = useState('');

  session.requireAuthentication(router);

  const showTimeRange = (marker?: Marker | null) => {
    if (marker?.startAt || marker?.finishAt) {
      return (
        <>
          <Typography>
            <span style={marker.startAt ? { marginRight: '12px' } : { display: 'none' }}>
              {i18n.t('startAt')} {new Date(marker.startAt).toLocaleString()}
            </span>
            <span style={marker.finishAt ? undefined : { display: 'none' }}>
              {i18n.t('finishAt')} {new Date(marker.finishAt).toLocaleString()}
            </span>
          </Typography>
        </>
      );
    }
    return <></>;
  };

  return (
    <BaseLayout title={i18n.t('yourMarkers')} noindex nofollow onSearchQueryChanged={(query) => setSearchQuery(query)}>
      <CurrentUserMarkersQueryComponent variables={{ query: searchQuery }}>
        {(result) => (
          <InfiniteScroll
            dataLength={result.data?.currentUser?.markers?.nodes?.length || 0}
            hasMore={result.loading || result.data?.currentUser?.markers?.pageInfo.hasNextPage || false}
            next={() =>
              result.fetchMore({
                variables: { after: result.data?.currentUser?.markers?.pageInfo?.endCursor },
                updateQuery: (prev, { fetchMoreResult }) => {
                  // eslint-disable-next-line prefer-object-spread
                  const newResult = Object.assign({}, fetchMoreResult);
                  if (newResult?.currentUser?.markers?.nodes) {
                    newResult.currentUser.markers.nodes = [
                      ...(prev.currentUser?.markers?.nodes || []),
                      ...(fetchMoreResult?.currentUser?.markers?.nodes || []),
                    ];
                  }
                  return newResult;
                },
              })
            }
            loader={
              <div style={{ display: 'flex' }}>
                <CircularProgress style={{ margin: '8px auto' }} />
              </div>
            }
          >
            {result.data?.currentUser?.markers?.nodes?.map((marker) => (
              <Link key={marker?.id} href="/your-markers/[id]" as={`/your-markers/${marker?.id}`} passHref>
                <a>
                  <Card className="item-card">
                    <CardActionArea>
                      <CardContent>
                        <Grid container justify="space-between">
                          <Grid item>
                            <Typography className="item-card-title">{marker?.name}</Typography>
                            <Typography>
                              {i18n.t('layer')}: <strong>{marker?.layer?.name}</strong>
                            </Typography>
                            {showTimeRange(marker)}
                          </Grid>
                          <Grid item>
                            <Typography hidden={marker?.published === true} color="textSecondary">
                              {i18n.t('unpublished')}
                            </Typography>
                          </Grid>
                        </Grid>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </a>
              </Link>
            ))}
          </InfiniteScroll>
        )}
      </CurrentUserMarkersQueryComponent>
    </BaseLayout>
  );
};
