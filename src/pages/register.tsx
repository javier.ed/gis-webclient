import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Button } from '@material-ui/core';
import { Formik, Form, Field, FormikValues } from 'formik';
import { TextField } from 'formik-material-ui';
import BaseLayout from '../layouts/base-layout';
import { useRegisterMutationMutation } from '../graphql/mutations/RegisterMutation.graphql';
import { AppContext } from './_app';
import settings from '../utils/settings';

export default () => {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [registerMutation] = useRegisterMutationMutation();

  session.requireNoAuthentication(router);
  if (typeof window !== 'undefined' && settings.registerDisabled()) {
    router.push('/', '/', { shallow: true });
  }

  const goBack = () => {
    router.push('/login', '/login', { shallow: true });
  };

  const goToLogin = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    router.push('/login', '/login', { shallow: true });
  };

  return (
    <BaseLayout title={i18n.t('register')} onBackButtonClick={goBack} noindex nofollow>
      <Formik
        initialValues={{
          username: '',
          email: '',
          password: '',
          passwordConfirmation: '',
        }}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (values.username.trim().length === 0) {
            errors.username = i18n.t('cantBeBlank');
          }
          if (values.email.trim().length === 0) {
            errors.email = i18n.t('cantBeBlank');
          }
          if (values.password.trim().length === 0) {
            errors.password = i18n.t('cantBeBlank');
          }
          if (values.passwordConfirmation.trim().length === 0) {
            errors.passwordConfirmation = i18n.t('cantBeBlank');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          registerMutation({
            variables: {
              attributes: {
                username: values.username,
                email: values.email,
                password: values.password,
                passwordConfirmation: values.passwordConfirmation,
              },
            },
          })
            .then((result) => {
              const register = result.data?.register;

              if (register?.success) {
                showSnackbar(register.message || '', 'success');
                session.start(
                  register.session?.id as string,
                  register.session?.key as string,
                  register.user?.id as string,
                  register.user?.username as string,
                );
                window.location.replace('/');
              } else {
                showSnackbar(register?.message || '', 'error');
                const errors: Partial<FormikValues> = {};
                register?.errors?.forEach((error: { path: string[]; message: string }) => {
                  errors[error.path[1]] = error.message;
                });
                setErrors(errors);
              }
              setSubmitting(false);
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form autoComplete="off">
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="username"
              label={i18n.t('username')}
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="email"
              label={i18n.t('email')}
              type="email"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label={i18n.t('password')}
              type="password"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="passwordConfirmation"
              label={i18n.t('passwordConfirmation')}
              type="password"
              autoComplete="off"
            />
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} fullWidth variant="contained" color="default">
              {i18n.t('register')}
            </Button>
          </Form>
        )}
      </Formik>

      <br />
      <br />

      <Button fullWidth variant="text" href="/login" onClick={goToLogin}>
        {i18n.t('iHaveAnAccount')}
      </Button>
    </BaseLayout>
  );
};
