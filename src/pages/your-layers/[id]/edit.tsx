import { useContext, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { FormikValues } from 'formik';
import { AppContext } from '../../_app';
import { useUpdateLayerMutationMutation } from '../../../graphql/mutations/UpdateLayerMutation.graphql';
import { LayerFieldsQueryComponent, Layer } from '../../../graphql/queries/LayerFieldsQuery.graphql';
import BaseLayout from '../../../layouts/base-layout';
import LayerForm from '../../../components/layer-form';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';
import { createApolloClient } from '../../../utils/apollo-client';

export default () => {
  const { showSnackbar, session, i18n, locale } = useContext(AppContext);
  const router = useRouter();
  const [layer, setLayer] = useState<Layer | null | undefined>();
  const [selectedLocale, setSelectedLocale] = useState(locale);
  const [apolloClient, setApolloClient] = useState(createApolloClient());

  session.requireAuthentication(router);

  useEffect(() => {
    setApolloClient(createApolloClient({ 'Accept-Language': selectedLocale }));
  }, [selectedLocale]);

  const [updateLayerMutation] = useUpdateLayerMutationMutation();

  const title = () => {
    if (layer) {
      return `${i18n.t('yourLayers')} > ${layer.name} > ${i18n.t('edit')}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (layer) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourLayers'),
              href: '/your-layers',
              className: 'app-bar-title',
            },
            {
              label: layer.name as string,
              href: '/your-layers/[id]',
              as: `/your-layers/${layer.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('edit'),
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <LayerFieldsQueryComponent
        variables={{ id: router.query.id as string }}
        skip={typeof router.query.id === 'undefined' || router.query.id == null}
        fetchPolicy="network-only"
        client={apolloClient}
      >
        {(result) => {
          setLayer(result.data?.layer);
          if (layer) {
            return (
              <LayerForm
                editing
                initialValues={{
                  locale: selectedLocale,
                  name: layer.name || '',
                  description: layer.description || '',
                  timeInterval: layer.timeInterval || 'any',
                  startAtCurrentTime: layer.startAtCurrentTime === true,
                  suggestions: layer.suggestions || 'requires_approval',
                  fields:
                    layer.fields?.nodes?.map((f) => ({
                      id: f?.id,
                      fieldType: f?.fieldType || '',
                      name: f?.name || '',
                      options: f?.options?.nodes?.map((o) => o?.name)?.join('\n'),
                      description: f?.description || '',
                      required: f?.required === true,
                      editable: f?.editable === true,
                      unique: f?.unique === true,
                    })) || [],
                }}
                onLocaleChange={setSelectedLocale}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  updateLayerMutation({
                    variables: {
                      id: layer.id || '',
                      attributes: {
                        locale: values.locale,
                        name: values.name,
                        description: values.description,
                        timeInterval: values.timeInterval,
                        startAtCurrentTime: !['any', 'disabled'].includes(values.timeInterval),
                        suggestions: values.suggestions,
                        fields: values.fields.map((field) => ({
                          id: field.id,
                          fieldType: !field.id ? field.fieldType : null,
                          name: field.name,
                          options: !field.id && field.fieldType === 'choice' ? field?.options?.split('\n') : null,
                          description: field.description,
                          required: field.required,
                          editable: field.editable,
                          unique: field.unique,
                          delete: field.delete,
                        })),
                      },
                    },
                  })
                    .then((r) => {
                      const updateLayer = r.data?.updateLayer;

                      if (updateLayer?.success) {
                        showSnackbar(updateLayer.message || '', 'success');
                        router.push({ pathname: '/', query: { layerId: updateLayer.layer?.id } });
                      } else {
                        showSnackbar(updateLayer?.message || '', 'error');
                        const errors: Partial<FormikValues> = { fields: [] };
                        updateLayer?.errors?.forEach((error: { path: string[]; message: string }) => {
                          if (error.path[1] === 'fields' && error.path.length === 4 && !isNaN(error.path[2] as any)) {
                            if (!errors.fields[error.path[2]]) {
                              errors.fields[error.path[2]] = {};
                            }
                            errors.fields[error.path[2]][error.path[3]] = error.message;
                          } else {
                            errors[error.path[1]] = error.message;
                          }
                        });
                        setErrors(errors);
                      }
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                      setSubmitting(false);
                    });
                }}
              />
            );
          }
          return <></>;
        }}
      </LayerFieldsQueryComponent>
    </BaseLayout>
  );
};
