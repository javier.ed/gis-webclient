import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Typography, Button } from '@material-ui/core';
import { Formik, FormikValues, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { CurrentUserLayerQueryComponent, Layer } from '../../../graphql/queries/CurrentUserLayerQuery.graphql';
import { AppContext } from '../../_app';
import BaseLayout from '../../../layouts/base-layout';
import { useDeleteLayerMutationMutation } from '../../../graphql/mutations/DeleteLayerMutation.graphql';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';

export default () => {
  const { showSnackbar, session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [layer, setLayer] = useState<Layer | null | undefined>();

  session.requireAuthentication(router);

  const [deleteLayerMutation] = useDeleteLayerMutationMutation();

  const title = () => {
    if (layer) {
      return `${i18n.t('yourLayers')} > ${layer.name} > ${i18n.t('delete')}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (layer) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourLayers'),
              href: '/your-layers',
              className: 'app-bar-title',
            },
            {
              label: layer.name as string,
              href: '/your-layers/[id]',
              as: `/your-layers/${layer.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('delete'),
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserLayerQueryComponent variables={{ id: router.query.id as string }}>
        {(result) => {
          setLayer(result.data?.currentUser?.layer);
          if (layer) {
            return (
              <Formik
                initialValues={{ name: '', confirm: false }}
                validate={(values) => {
                  const errors: Partial<FormikValues> = {};
                  if (values.name !== layer.name) {
                    errors.name = i18n.t('isInvalid');
                  }
                  return errors;
                }}
                onSubmit={(_values, { setSubmitting }) => {
                  deleteLayerMutation({ variables: { id: layer.id } })
                    .then((r) => {
                      const deleteLayer = r.data?.deleteLayer;

                      if (deleteLayer?.success) {
                        showSnackbar(deleteLayer.message || '', 'success');
                        router.push('/', '/');
                      } else {
                        showSnackbar(deleteLayer?.message || '', 'error');
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                      setSubmitting(false);
                    });
                }}
              >
                {({ submitForm, isSubmitting }) => (
                  <Form autoComplete="off">
                    <Typography>{i18n.t('enterTheNameOfTheLayerYouWantToDelete')}</Typography>
                    <Field
                      component={TextField}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      name="name"
                      label={i18n.t('name')}
                    />
                    <br />
                    <br />
                    <Button disabled={isSubmitting} onClick={submitForm} variant="contained" color="default" fullWidth>
                      {i18n.t('delete')}
                    </Button>
                  </Form>
                )}
              </Formik>
            );
          }
          return <></>;
        }}
      </CurrentUserLayerQueryComponent>
    </BaseLayout>
  );
};
