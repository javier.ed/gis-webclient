import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Card, CardActionArea, CardContent, Typography, CircularProgress } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  CurrentUserLayerPendingMarkersQueryComponent,
  Layer,
  Marker,
} from '../../../../graphql/queries/CurrentUserLayerPendingMarkersQuery.graphql';
import { AppContext } from '../../../_app';
import BaseLayout from '../../../../layouts/base-layout';
import ResponsiveBreadcrumbs from '../../../../components/responsive-breadcrumbs';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [layer, setLayer] = useState<Layer | null | undefined>();

  session.requireAuthentication(router);

  const title = () => {
    if (layer) {
      return `${i18n.t('yourLayers')} > ${layer.name} > ${i18n.t('pendingMarkers')}`;
    }
    return undefined;
  };

  const showTimeRange = (marker?: Marker | null) => {
    if (marker?.startAt || marker?.finishAt) {
      return (
        <>
          <Typography>
            <span style={marker.startAt ? { marginRight: '12px' } : { display: 'none' }}>
              {i18n.t('startAt')} {new Date(marker.startAt).toLocaleString()}
            </span>
            <span style={marker.finishAt ? undefined : { display: 'none' }}>
              {i18n.t('finishAt')} {new Date(marker.finishAt).toLocaleString()}
            </span>
          </Typography>
        </>
      );
    }
    return <></>;
  };

  const appBarTitle = () => {
    if (layer) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourLayers'),
              href: '/your-layers',
              className: 'app-bar-title',
            },
            {
              label: layer.name as string,
              href: '/your-layers/[id]',
              as: `/your-layers/${layer.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('pendingMarkers'),
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserLayerPendingMarkersQueryComponent variables={{ id: router.query.id as string }}>
        {(result) => {
          setLayer(result.data?.currentUser?.layer);
          return (
            <InfiniteScroll
              dataLength={layer?.pendingMarkers?.nodes?.length || 0}
              hasMore={result.loading || layer?.pendingMarkers?.pageInfo.hasNextPage || false}
              next={() =>
                result.fetchMore({
                  variables: { after: layer?.pendingMarkers?.pageInfo?.endCursor },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    // eslint-disable-next-line prefer-object-spread
                    const newResult = Object.assign({}, fetchMoreResult);
                    if (newResult?.currentUser?.layer?.pendingMarkers?.nodes) {
                      newResult.currentUser.layer.pendingMarkers.nodes = [
                        ...(prev.currentUser?.layer?.pendingMarkers?.nodes || []),
                        ...(fetchMoreResult?.currentUser?.layer?.pendingMarkers?.nodes || []),
                      ];
                    }
                    return newResult;
                  },
                })
              }
              loader={
                <div style={{ display: 'flex' }}>
                  <CircularProgress style={{ margin: '8px auto' }} />
                </div>
              }
            >
              {layer?.pendingMarkers?.nodes?.map((marker) => (
                <Link
                  key={marker?.id}
                  href="/your-layers/[id]/pending-markers/[markerId]"
                  as={`/your-layers/${layer?.id}/pending-markers/${marker?.id}`}
                  passHref
                >
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{marker?.name}</Typography>
                          {showTimeRange(marker)}
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
              ))}
            </InfiniteScroll>
          );
        }}
      </CurrentUserLayerPendingMarkersQueryComponent>
    </BaseLayout>
  );
};
