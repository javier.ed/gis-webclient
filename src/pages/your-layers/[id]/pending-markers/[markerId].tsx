import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Formik, Form, FormikValues, Field } from 'formik';
import { Button, FormControlLabel } from '@material-ui/core';
import { TextField, Switch } from 'formik-material-ui';
import BaseLayout from '../../../../layouts/base-layout';
import { AppContext } from '../../../_app';
import { MarkerQueryComponent, Marker as LayerMarker } from '../../../../graphql/queries/MarkerQuery.graphql';
import { MapWithTileLayer } from '../../../../components/react-leaflet';
import SelectedMarker from '../../../../components/selected-marker';
import ResponsiveBreadcrumbs from '../../../../components/responsive-breadcrumbs';
// eslint-disable-next-line max-len
import { useCreateMarkerApprovalMutationMutation } from '../../../../graphql/mutations/CreateMarkerApprovalMutation.graphql';

export default () => {
  const { showSnackbar, session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [marker, setMarker] = useState<LayerMarker | null | undefined>();

  session.requireAuthentication(router);

  const [createMarkerApprovalMutation] = useCreateMarkerApprovalMutationMutation();

  const title = () => {
    if (marker) {
      return `${i18n.t('yourLayers')} > ${marker.layer?.name} > ${i18n.t('pendingMarkers')} > ${marker.name}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (marker) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            {
              label: i18n.t('yourLayers'),
              href: '/your-layers',
              className: 'app-bar-title',
            },
            {
              label: marker.layer?.name as string,
              href: '/your-layers/[id]',
              as: `/your-layers/${marker.layer?.id}`,
              className: 'app-bar-title',
            },
            {
              label: i18n.t('pendingMarkers'),
              href: '/your-layers/[id]/pending-markers',
              as: `/your-layers/${marker.layer?.id}/pending-markers`,
              className: 'app-bar-title',
            },
            {
              label: marker.name as string,
              className: 'app-bar-title',
            },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <MarkerQueryComponent variables={{ id: router.query.markerId as string }}>
        {(result) => {
          setMarker(result.data?.marker);
          if (marker) {
            return (
              <>
                <MapWithTileLayer
                  center={[marker.geocodingPoint?.latitude || 0, marker.geocodingPoint?.longitude || 0]}
                  style={{ height: 'calc(100vh - 100px)' }}
                >
                  <SelectedMarker value={marker} update={setMarker} />
                </MapWithTileLayer>

                <br />
                <br />

                <Formik
                  initialValues={{ approved: false, comment: '' }}
                  validate={(values) => {
                    const errors: Partial<FormikValues> = {};
                    if (!values.approved && values.comment.trim().length === 0) {
                      errors.comment = i18n.t('cantBeBlank');
                    }
                    return errors;
                  }}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    if (
                      // eslint-disable-next-line no-alert
                      confirm(
                        values.approved
                          ? i18n.t('areYouSureYouWantToApproveThisMarker')
                          : i18n.t('areYouSureYouWantToRejectThisMarker'),
                      )
                    ) {
                      createMarkerApprovalMutation({
                        variables: {
                          attributes: {
                            markerId: marker.id,
                            approved: values.approved,
                            comment: values.comment,
                          },
                        },
                      })
                        .then((r) => {
                          const createMarkerApproval = r.data?.createMarkerApproval;

                          if (createMarkerApproval?.success) {
                            showSnackbar(createMarkerApproval.message || '', 'success');
                            router.push(
                              '/your-layers/[id]/pending-markers',
                              `/your-layers/${marker.layer?.id}/pending-markers`,
                            );
                          } else {
                            showSnackbar(createMarkerApproval?.message || '', 'error');
                            const errors: Partial<FormikValues> = {};
                            createMarkerApproval?.errors?.forEach((error: { path: string[]; message: string }) => {
                              errors[error.path[1]] = error.message;
                            });
                            setErrors(errors);
                          }
                          setSubmitting(false);
                        })
                        .catch(() => {
                          showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                          setSubmitting(false);
                        });
                    } else {
                      setSubmitting(false);
                    }
                  }}
                >
                  {({ submitForm, isSubmitting, values }) => (
                    <Form autoComplete="off">
                      <div>
                        <FormControlLabel
                          label={i18n.t('approved')}
                          labelPlacement="start"
                          control={<Field component={Switch} name="approved" type="checkbox" />}
                        />
                      </div>
                      <Field
                        component={TextField}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="comment"
                        label={i18n.t('comment')}
                        multiline
                        required={!values.approved}
                        rows={2}
                        rowsMax={4}
                      />
                      <br />
                      <br />
                      <Button
                        disabled={isSubmitting}
                        onClick={submitForm}
                        variant="contained"
                        color="default"
                        fullWidth
                      >
                        {i18n.t('submit')}
                      </Button>
                      <br />
                      <br />
                    </Form>
                  )}
                </Formik>
              </>
            );
          }
          return <></>;
        }}
      </MarkerQueryComponent>
    </BaseLayout>
  );
};
