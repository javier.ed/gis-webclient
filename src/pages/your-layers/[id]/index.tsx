import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Card, CardActionArea, CardContent, Typography } from '@material-ui/core';
import { CurrentUserLayerQueryComponent, Layer } from '../../../graphql/queries/CurrentUserLayerQuery.graphql';
import { AppContext } from '../../_app';
import BaseLayout from '../../../layouts/base-layout';
import ResponsiveBreadcrumbs from '../../../components/responsive-breadcrumbs';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [layer, setLayer] = useState<Layer | null | undefined>();

  session.requireAuthentication(router);

  const title = () => {
    if (layer) {
      return `${i18n.t('yourLayers')} > ${layer.name}`;
    }
    return undefined;
  };

  const appBarTitle = () => {
    if (layer) {
      return (
        <ResponsiveBreadcrumbs
          items={[
            { label: i18n.t('yourLayers'), href: '/your-layers', className: 'app-bar-title' },
            { label: layer.name as string, className: 'app-bar-title' },
          ]}
        />
      );
    }
    return undefined;
  };

  return (
    <BaseLayout title={title()} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserLayerQueryComponent variables={{ id: router.query.id as string }}>
        {(result) => {
          setLayer(result.data?.currentUser?.layer);
          if (layer) {
            return (
              <>
                <Link href={`/?layerId=${layer.id}`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('view')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
                <Link href="/your-layers/[id]/edit" as={`/your-layers/${layer.id}/edit`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('edit')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
                <Link href="/your-layers/[id]/pending-markers" as={`/your-layers/${layer.id}/pending-markers`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('pendingMarkers')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
                <Link href="/your-layers/[id]/delete" as={`/your-layers/${layer.id}/delete`} passHref>
                  <a>
                    <Card className="item-card">
                      <CardActionArea>
                        <CardContent>
                          <Typography className="item-card-title">{i18n.t('delete')}</Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </a>
                </Link>
              </>
            );
          }
          return <></>;
        }}
      </CurrentUserLayerQueryComponent>
    </BaseLayout>
  );
};
