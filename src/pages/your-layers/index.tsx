import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Typography, CircularProgress, Card, CardActionArea, CardContent } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';
import Link from 'next/link';
import BaseLayout from '../../layouts/base-layout';
import { CurrentUserLayersQueryComponent } from '../../graphql/queries/CurrentUserLayersQuery.graphql';
import { AppContext } from '../_app';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();
  const [searchQuery, setSearchQuery] = useState('');

  session.requireAuthentication(router);

  return (
    <BaseLayout title={i18n.t('yourLayers')} noindex nofollow onSearchQueryChanged={(query) => setSearchQuery(query)}>
      <CurrentUserLayersQueryComponent variables={{ query: searchQuery }}>
        {(result) => (
          <InfiniteScroll
            dataLength={result.data?.currentUser?.layers?.nodes?.length || 0}
            hasMore={result.loading || result.data?.currentUser?.layers?.pageInfo.hasNextPage || false}
            next={() =>
              result.fetchMore({
                variables: { after: result.data?.currentUser?.layers?.pageInfo?.endCursor },
                updateQuery: (prev, { fetchMoreResult }) => {
                  // eslint-disable-next-line prefer-object-spread
                  const newResult = Object.assign({}, fetchMoreResult);
                  if (newResult?.currentUser?.layers?.nodes) {
                    newResult.currentUser.layers.nodes = [
                      ...(prev.currentUser?.layers?.nodes || []),
                      ...(fetchMoreResult?.currentUser?.layers?.nodes || []),
                    ];
                  }
                  return newResult;
                },
              })
            }
            loader={
              <div style={{ display: 'flex' }}>
                <CircularProgress style={{ margin: '8px auto' }} />
              </div>
            }
          >
            {result.data?.currentUser?.layers?.nodes?.map((layer) => (
              <Link key={layer?.id} href="/your-layers/[id]" as={`/your-layers/${layer?.id}`} passHref>
                <a>
                  <Card className="item-card">
                    <CardActionArea>
                      <CardContent>
                        <Typography className="item-card-title">{layer?.name}</Typography>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </a>
              </Link>
            ))}
          </InfiniteScroll>
        )}
      </CurrentUserLayersQueryComponent>
    </BaseLayout>
  );
};
