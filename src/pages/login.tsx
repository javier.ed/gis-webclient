import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Button } from '@material-ui/core';
import { Formik, Form, Field, FormikValues } from 'formik';
import { TextField } from 'formik-material-ui';
import BaseLayout from '../layouts/base-layout';
import { useLoginMutationMutation } from '../graphql/mutations/LoginMutation.graphql';
import { AppContext } from './_app';
import settings from '../utils/settings';

export default () => {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [loginMutation] = useLoginMutationMutation();

  session.requireNoAuthentication(router);

  const goBack = () => {
    router.push('/', '/', { shallow: true });
  };

  const goToRegister = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    router.push('/register', '/register', { shallow: true });
  };

  const goToRequestPasswordReset = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    router.push('/request-password-reset', '/request-password-reset', { shallow: true });
  };

  return (
    <BaseLayout title={i18n.t('login')} onBackButtonClick={goBack} noindex nofollow>
      <Formik
        initialValues={{
          login: '',
          password: '',
        }}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (values.login.trim().length === 0) {
            errors.login = i18n.t('cantBeBlank');
          }
          if (values.password.trim().length === 0) {
            errors.password = i18n.t('cantBeBlank');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          loginMutation({ variables: { login: values.login, password: values.password } })
            .then((result) => {
              const login = result.data?.login;
              if (login?.success) {
                showSnackbar(login.message || '', 'success');
                session.start(
                  login.session?.id as string,
                  login.session?.key as string,
                  login.user?.id as string,
                  login.user?.username as string,
                );
                window.location.replace('/');
              } else {
                showSnackbar(login?.message || '', 'error');
              }
              setSubmitting(false);
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form autoComplete="off">
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label={i18n.t('usernameOrEmail')}
              name="login"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label={i18n.t('password')}
              type="password"
              autoComplete="off"
            />
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} fullWidth variant="contained" color="default">
              {i18n.t('login')}
            </Button>
          </Form>
        )}
      </Formik>

      <br />
      <br />

      <Button
        fullWidth
        variant="text"
        href="/register"
        onClick={goToRegister}
        style={settings.registerDisabled() ? { display: 'none' } : undefined}
      >
        {i18n.t('iDontHaveAnAccount')}
      </Button>

      <br />
      <br />

      <Button fullWidth variant="text" href="/request-password-reset" onClick={goToRequestPasswordReset}>
        {i18n.t('iForgotMyPassword')}
      </Button>
    </BaseLayout>
  );
};
