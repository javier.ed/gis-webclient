import { useContext } from 'react';
import { useRouter } from 'next/router';
import { FormikValues } from 'formik';
import BaseLayout from '../layouts/base-layout';
import { AppContext } from './_app';
import { useCreateLayerMutationMutation } from '../graphql/mutations/CreateLayerMutation.graphql';
import LayerForm, { defaultFieldValues } from '../components/layer-form';

export default () => {
  const { showSnackbar, session, i18n, locale } = useContext(AppContext);
  const router = useRouter();

  session.requireAuthentication(router);

  const [createLayerMutation] = useCreateLayerMutationMutation();

  return (
    <BaseLayout title={i18n.t('newLayer')} noindex nofollow>
      <LayerForm
        initialValues={{
          locale,
          name: '',
          description: '',
          timeInterval: 'any',
          startAtCurrentTime: false,
          suggestions: 'requires_approval',
          fields: [defaultFieldValues],
        }}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          createLayerMutation({
            variables: {
              attributes: {
                locale: values.locale,
                name: values.name,
                description: values.description,
                timeInterval: values.timeInterval,
                startAtCurrentTime: !['any', 'disabled'].includes(values.timeInterval),
                suggestions: values.suggestions,
                fields: values.fields.map((field) =>
                  Object.assign(field, { options: field.fieldType === 'choice' ? field.options?.split('\n') : null }),
                ),
                translate: values.translate,
              },
            },
          })
            .then((result) => {
              const createLayer = result.data?.createLayer;

              if (createLayer?.success) {
                showSnackbar(createLayer.message || '', 'success');
                router.push({ pathname: '/', query: { layerId: createLayer.layer?.id } });
              } else {
                showSnackbar(createLayer?.message || '', 'error');
                const errors: Partial<FormikValues> = { fields: [] };
                createLayer?.errors?.forEach((error: { path: string[]; message: string }) => {
                  if (error.path[1] === 'fields' && error.path.length === 4 && !isNaN(error.path[2] as any)) {
                    if (!errors.fields[error.path[2]]) {
                      errors.fields[error.path[2]] = {};
                    }
                    errors.fields[error.path[2]][error.path[3]] = error.message;
                  } else {
                    errors[error.path[1]] = error.message;
                  }
                });
                setErrors(errors);
              }
              setSubmitting(false);
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
              setSubmitting(false);
            });
        }}
      />
    </BaseLayout>
  );
};
