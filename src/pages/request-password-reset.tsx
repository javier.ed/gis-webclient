import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Formik, FormikValues, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { Button } from '@material-ui/core';
import { useRequestPasswordResetMutationMutation } from '../graphql/mutations/RequestPasswordResetMutation.graphql';
import BaseLayout from '../layouts/base-layout';
import { AppContext } from './_app';

export default () => {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [requestPasswordResetMutation] = useRequestPasswordResetMutationMutation();

  session.requireNoAuthentication(router);

  const goBack = () => {
    router.push('/login', '/login', { shallow: true });
  };

  const goToLogin = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    router.push('/login', '/login', { shallow: true });
  };

  return (
    <BaseLayout title={i18n.t('requestPasswordReset')} onBackButtonClick={goBack} noindex nofollow>
      <Formik
        initialValues={{ login: '' }}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (values.login.trim().length === 0) {
            errors.login = i18n.t('cantBeBlank');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          requestPasswordResetMutation({ variables: { login: values.login } })
            .then((result) => {
              const requestPasswordReset = result.data?.requestPasswordReset;

              if (requestPasswordReset?.success) {
                showSnackbar(requestPasswordReset.message || '', 'success');
                router.push({ pathname: '/reset-password', query: { login: values.login } }, undefined, {
                  shallow: true,
                });
              } else {
                showSnackbar(requestPasswordReset?.message || '', 'error');
              }
              setSubmitting(false);
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest') || '', 'error');
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form autoComplete="off">
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label={i18n.t('usernameOrEmail')}
              name="login"
              autoComplete="off"
            />
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} fullWidth variant="contained" color="default">
              {i18n.t('requestPasswordReset')}
            </Button>
          </Form>
        )}
      </Formik>

      <br />
      <br />

      <Button fullWidth variant="text" href="/login" onClick={goToLogin}>
        {i18n.t('goToLogin')}
      </Button>
    </BaseLayout>
  );
};
