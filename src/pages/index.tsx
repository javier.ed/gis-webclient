import React, { useState, useContext, useEffect } from 'react';
import Head from 'next/head';
import {
  AppBar,
  Toolbar,
  Typography,
  ThemeProvider,
  CssBaseline,
  IconButton,
  makeStyles,
  createStyles,
  Hidden,
} from '@material-ui/core';
import { NextPageContext } from 'next';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import Link from 'next/link';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { AppContext } from './_app';
import { MapWithTileLayer } from '../components/react-leaflet';
import SessionMenuItem from '../components/session-menu-item';
import theme from '../utils/theme';
import { MarkerQueryDocument, Marker as LayerMarker } from '../graphql/queries/MarkerQuery.graphql';
import { LayerQueryDocument, Layer } from '../graphql/queries/LayerQuery.graphql';
import apolloClient from '../utils/apollo-client';
import SelectedMarker from '../components/selected-marker';
import SearchMenuItem from '../components/search-menu-item';
import DateTimeSelector from '../components/index/date-time-selector';
import CurrentLayer from '../components/index/current-layer';
import SearchMarkers from '../components/index/search-markers';
import LocateMarkers from '../components/index/locate-markers';
import LocateFavoriteMarkers from '../components/index/locate-favorite-markers';
import LayersSubscriptions from '../components/index/layers-subscriptions';

let timeoutMapChangeId: any;

export default function Index(props: { initialLayer: Layer | null; initialMarker: LayerMarker | null }) {
  const { showSnackbar, i18n, getCurrentPosition } = useContext(AppContext);
  const router = useRouter();
  const [initialCenter, setInitialCenter] = useState<[number, number]>(() => {
    const c = router.query.center?.toString()?.split(',');
    return c && c.length === 2 && !isNaN(Number(c[0])) && !isNaN(Number(c[1])) ? [Number(c[0]), Number(c[1])] : [0, 0];
  });
  const [initialZoom, setInitialZoom] = useState(isNaN(Number(router.query.zoom)) ? 13 : Number(router.query.zoom));
  const [additionalLayer, setAdditionalLayer] = useState<Layer | null>(props.initialLayer);
  const [currentLayer, setCurrentLayer] = useState<Layer | null>(props.initialLayer);
  const [locateMarkersVariables, setLocateMarkersVariables] = useState({
    latitude: initialCenter[0],
    longitude: initialCenter[1],
    area: 50,
  });
  const [currentMarker, setCurrentMarker] = useState<LayerMarker | null | undefined>(props.initialMarker);
  const [currentCenter, setCurrentCenter] = useState(initialCenter);
  const [currentZoom, setCurrentZoom] = useState(initialZoom);
  const [openSearchResultsDrawer, setOpenSearchResultsDrawer] = useState(false);
  const [currentDatetime, setCurrentDatetime] = useState<Date | null>(
    router.query.dateAndTime ? new Date(router.query.dateAndTime as string) : null,
  );
  const [searchQuery, setSearchQuery] = useState('');
  const [locateFavorites, setLocateFavorites] = useState(router.query.favorites === '1');

  const markerLatitudeDiffToCenter = 0.00133;

  const matchesXsDown = useMediaQuery(theme.breakpoints.down('xs'));

  const useStyles = makeStyles(() =>
    createStyles({
      appBar: {
        zIndex: theme.zIndex.drawer + 1,
        backgroundColor: '#DDDDDD99 !important',
      },
    }),
  );
  const classes = useStyles();

  const setInitialCenterInCurrentPosition = () => {
    if (initialCenter[0] === 0 && initialCenter[1] === 0) {
      getCurrentPosition((value) => {
        setInitialCenter([value.coords.latitude, value.coords.longitude]);
      });
    }
  };

  const loadInitialLayer = () => {
    if (router.query.layerId && router.query.layerId !== currentLayer?.id) {
      showSnackbar(i18n.t('layerNotFound'), 'error');
    }
  };

  const locateMarkerInDatetime = (marker: LayerMarker) => {
    const dateTime = currentDatetime || new Date();
    if (marker.finishAt) {
      const finishAt = new Date(marker.finishAt);
      if (dateTime >= finishAt) {
        const newDate = new Date(marker.finishAt);
        newDate.setSeconds(newDate.getSeconds() - 1);
        setCurrentDatetime(newDate);
        return;
      }
    }
    if (marker.startAt) {
      const startAt = new Date(marker.startAt);
      if (dateTime < startAt) {
        setCurrentDatetime(startAt);
      }
    }
  };

  const locateInitialMarker = () => {
    if (router.query.markerId) {
      if (router.query.markerId === currentMarker?.id) {
        setInitialCenter([
          (currentMarker?.geocodingPoint?.latitude as number) + markerLatitudeDiffToCenter,
          currentMarker?.geocodingPoint?.longitude as number,
        ]);
        setInitialZoom(17);
        locateMarkerInDatetime(currentMarker);
      } else {
        showSnackbar(i18n.t('markerNotFound'), 'error');
        setInitialCenterInCurrentPosition();
      }
    } else {
      setInitialCenterInCurrentPosition();
    }
  };

  useEffect(() => {
    loadInitialLayer();
    locateInitialMarker();
  }, []);

  useEffect(() => {
    setCurrentCenter(initialCenter);
    setCurrentZoom(initialZoom);
  }, [initialCenter, initialZoom]);

  const selectLayer = (layer: Layer | null) => {
    setCurrentMarker(null);
    setCurrentLayer(layer);
    setLocateFavorites(false);
  };

  const toggleFavorites = () => {
    selectLayer(null);
    setLocateFavorites(!locateFavorites);
  };

  useEffect(() => {
    const query: {
      layerId?: string;
      markerId?: string;
      center?: string;
      zoom?: number;
      dateAndTime?: string;
      favorites?: number;
    } = {};
    if (locateFavorites) {
      query.favorites = 1;
      if (currentMarker?.id) {
        query.markerId = currentMarker.id;
      }
    } else if (currentMarker?.id) {
      query.markerId = currentMarker.id;
    } else if (currentLayer?.id) {
      query.layerId = currentLayer.id;
    }
    if (currentDatetime) {
      query.dateAndTime = `${currentDatetime.toISOString().substr(0, 19)}Z`;
    }
    query.center = currentCenter.join(',');
    query.zoom = currentZoom;
    router.replace({ pathname: '/', query }, undefined, { shallow: true });
  }, [currentMarker, currentLayer, currentCenter, currentZoom, currentDatetime, locateFavorites]);

  const selectMarker = (marker?: LayerMarker | null) => {
    setCurrentMarker(marker);
    if (marker && !locateFavorites) {
      locateMarkerInDatetime(marker);
    }
  };

  const changeMapValues = (event: any) => {
    const { lat, lng } = event.target.getCenter();
    const area =
      event.target.distance(event.target.getBounds().getNorthEast(), event.target.getBounds().getSouthWest()) / 1000;
    setLocateMarkersVariables({ latitude: lat, longitude: lng, area });
    setCurrentCenter([lat, lng]);
    setCurrentZoom(event.target.getZoom());
  };

  const handleMapChange = (event: any) => {
    if (typeof timeoutMapChangeId !== 'undefined') {
      clearTimeout(timeoutMapChangeId);
    }
    timeoutMapChangeId = setTimeout(changeMapValues, 500, event);
  };

  const selectMarkerFromSearch = (marker: LayerMarker) => {
    if (!marker.layer?.currentUserSubscription?.id) {
      setAdditionalLayer(marker?.layer || null);
    }
    setCurrentLayer(marker?.layer || null);
    selectMarker(marker);
    setInitialZoom(17);
    if (matchesXsDown) {
      setInitialCenter([
        (marker.geocodingPoint?.latitude as number) + markerLatitudeDiffToCenter,
        marker.geocodingPoint?.longitude as number,
      ]);
      setOpenSearchResultsDrawer(false);
    } else {
      setInitialCenter([
        (marker.geocodingPoint?.latitude as number) + markerLatitudeDiffToCenter,
        (marker.geocodingPoint?.longitude as number) + 0.00193,
      ]);
    }
  };

  const pageTitle = () => {
    let title = '';
    if (currentLayer) {
      title += currentLayer.name;
      if (currentMarker) {
        title += ` > ${currentMarker.name}`;
      }
      if (currentDatetime) {
        title += ` (${currentDatetime.toLocaleString()})`;
      }
      return `${title} | Bululú`;
    }
    return `Bululú - ${i18n.t('appDescription')}`;
  };

  const pageDescription = () => {
    let description = '';
    if (currentMarker) {
      description += currentMarker.name;
    } else if (currentLayer) {
      description += currentLayer.description;
    } else {
      description += i18n.t('appDescription');
    }
    return description;
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Head>
        <title>{pageTitle()}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NextSeo
        title={pageTitle()}
        description={pageDescription()}
        openGraph={{
          title: pageTitle(),
          description: pageDescription(),
        }}
      />
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Link href="/" passHref>
            <IconButton
              className="app-bar-icon-container"
              onClick={(e) => {
                e.preventDefault();
                selectLayer(null);
              }}
            >
              <img src="/icon.svg" alt="Logo" />
            </IconButton>
          </Link>
          <Hidden xsDown>
            <Typography className="app-bar-title app-bar-title-home" color="textPrimary">
              Bululú
            </Typography>
          </Hidden>
          <div style={{ flexGrow: 1 }} />
          <SearchMenuItem
            onQueryChanged={(query) => setSearchQuery(query)}
            onFocus={() => setOpenSearchResultsDrawer(true)}
          />
          <SessionMenuItem />
        </Toolbar>
      </AppBar>

      <SearchMarkers
        query={searchQuery}
        openDrawer={openSearchResultsDrawer}
        onCloseDrawer={() => setOpenSearchResultsDrawer(false)}
        onMarkerClick={selectMarkerFromSearch}
      />

      <MapWithTileLayer
        center={initialCenter}
        zoom={initialZoom}
        onLoad={handleMapChange}
        onMoveEnd={handleMapChange}
        onResize={handleMapChange}
        onZoomEnd={handleMapChange}
        onClick={(e) => {
          if (e.originalEvent.target.classList.contains('leaflet-container')) {
            setCurrentMarker(null);
          }
        }}
      >
        <LocateFavoriteMarkers
          variables={locateMarkersVariables}
          skip={!locateFavorites}
          onClickMarker={selectMarker}
        />

        <LocateMarkers
          variables={{ layerId: currentLayer?.id, dateAndTime: currentDatetime, ...locateMarkersVariables }}
          skip={currentLayer == null || locateFavorites}
          currentMarker={currentMarker}
          onClickMarker={selectMarker}
        />

        <SelectedMarker value={currentMarker} onClick={() => setCurrentMarker(null)} update={setCurrentMarker} />
      </MapWithTileLayer>

      <CurrentLayer
        value={currentLayer}
        update={setCurrentLayer}
        setAdditionalLayer={setAdditionalLayer}
        onClose={() => selectLayer(null)}
        favorites={locateFavorites}
      />

      <DateTimeSelector
        value={currentDatetime}
        onChange={setCurrentDatetime}
        hidden={!currentLayer || currentLayer.timeInterval === 'disabled'}
        disabled={typeof currentMarker !== 'undefined' && currentMarker !== null}
      />

      <LayersSubscriptions
        locateFavorites={locateFavorites}
        onClickFavorites={toggleFavorites}
        additionalLayer={additionalLayer}
        currentLayer={currentLayer}
        onClickLayer={selectLayer}
      />
    </ThemeProvider>
  );
}

Index.getInitialProps = async (pageContext: NextPageContext) => {
  let initialLayer: any;
  let initialMarker: any;
  const apollo = apolloClient();
  if (pageContext.query.markerId) {
    const markerQuery = await apollo.query({
      query: MarkerQueryDocument,
      variables: { id: pageContext.query.markerId },
    });
    initialMarker = markerQuery?.data?.marker;
    if (pageContext.query.favorites !== '1') {
      initialLayer = initialMarker?.layer;
    }
  } else if (pageContext.query.layerId) {
    const layerQuery = await apollo.query({
      query: LayerQueryDocument,
      variables: { id: pageContext.query.layerId },
    });
    initialLayer = layerQuery?.data?.layer;
  }
  return { initialLayer, initialMarker };
};
