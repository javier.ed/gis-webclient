import { useContext, useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  CardActionArea,
  CircularProgress,
  Grid,
} from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { useRouter } from 'next/router';
import InfiniteScroll from 'react-infinite-scroll-component';
import Link from 'next/link';
import { LayersQueryComponent } from '../graphql/queries/LayersQuery.graphql';
import { AppContext } from './_app';
import BaseLayout from '../layouts/base-layout';

export default () => {
  const { i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null);
  const [searchQuery, setSearchQuery] = useState('');

  const goToNewLayer = () => {
    setAnchorMenu(null);
    router.push('/new-layer', '/new-layer', { shallow: true });
  };

  let menu = <div />;

  if (session.exists()) {
    menu = (
      <div>
        <IconButton onClick={(event) => setAnchorMenu(event.currentTarget)}>
          <MoreVert />
        </IconButton>
        <Menu anchorEl={anchorMenu} open={Boolean(anchorMenu)} onClose={() => setAnchorMenu(null)}>
          <MenuItem onClick={goToNewLayer}>{i18n.t('newLayer')}</MenuItem>
        </Menu>
      </div>
    );
  }

  return (
    <BaseLayout title={i18n.t('layers')} menu={menu} onSearchQueryChanged={(query) => setSearchQuery(query)}>
      <LayersQueryComponent variables={{ query: searchQuery }}>
        {(result) => (
          <InfiniteScroll
            dataLength={result.data?.layers?.nodes?.length || 0}
            hasMore={result.loading || result.data?.layers?.pageInfo.hasNextPage || false}
            next={() =>
              result.fetchMore({
                variables: {
                  after: result.data?.layers?.pageInfo?.endCursor,
                },
                updateQuery: (prev, { fetchMoreResult }) => {
                  // eslint-disable-next-line prefer-object-spread
                  const newResult = Object.assign({}, fetchMoreResult);
                  if (newResult.layers?.nodes) {
                    newResult.layers.nodes = [...(prev.layers?.nodes || []), ...(fetchMoreResult?.layers?.nodes || [])];
                  }
                  return newResult;
                },
              })
            }
            loader={
              <div style={{ display: 'flex' }}>
                <CircularProgress style={{ margin: '8px auto' }} />
              </div>
            }
          >
            {result.data?.layers?.nodes?.map((layer) => (
              <Link key={layer?.id} href={`/?layerId=${layer?.id}`} passHref>
                <a>
                  <Card className="item-card">
                    <CardActionArea>
                      <CardContent>
                        <Grid container justify="space-between">
                          <Grid item>
                            <Typography className="item-card-title">{layer?.name}</Typography>
                            <Typography>{layer?.description}</Typography>
                          </Grid>
                          <Grid item>
                            <Typography hidden={!layer?.currentUserSubscription?.id} color="textSecondary">
                              {i18n.t('subscribed')}
                            </Typography>
                          </Grid>
                        </Grid>
                      </CardContent>
                    </CardActionArea>
                  </Card>
                </a>
              </Link>
            ))}
          </InfiniteScroll>
        )}
      </LayersQueryComponent>
    </BaseLayout>
  );
};
