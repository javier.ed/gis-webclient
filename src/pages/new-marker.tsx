import { useContext, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { FormikValues } from 'formik';
import { AppContext } from './_app';
import BaseLayout from '../layouts/base-layout';
import { useCreateMarkerMutationMutation } from '../graphql/mutations/CreateMarkerMutation.graphql';
import { LayerFieldsQueryComponent } from '../graphql/queries/LayerFieldsQuery.graphql';
import MarkerForm from '../components/marker-form';
import { fixDateLocale } from '../utils/date-helpers';
import { createApolloClient } from '../utils/apollo-client';

export default () => {
  const { showSnackbar, session, i18n, locale } = useContext(AppContext);
  const router = useRouter();
  const [selectedLocale, setSelectedLocale] = useState(locale);
  const [apolloClient, setApolloClient] = useState(createApolloClient());

  session.requireAuthentication(router);

  useEffect(() => {
    setApolloClient(createApolloClient({ 'Accept-Language': selectedLocale }));
  }, [selectedLocale]);

  const [createMarkerMutation] = useCreateMarkerMutationMutation();

  return (
    <BaseLayout title={i18n.t('newMarker')} noindex nofollow>
      <LayerFieldsQueryComponent
        variables={{ id: router.query.layerId as string }}
        skip={typeof router.query.layerId === 'undefined' || router.query.layerId == null}
        fetchPolicy="network-only"
        client={apolloClient}
      >
        {(result) => {
          const layer = result.data?.layer;
          if (layer) {
            return (
              <MarkerForm
                layer={{
                  id: layer.id,
                  name: layer.name,
                  timeInterval: layer.timeInterval,
                  startAtCurrentTime: layer.startAtCurrentTime,
                  suggestions: layer.suggestions,
                  fields: layer.fields?.nodes?.map((f) => ({
                    id: f?.id,
                    fieldType: f?.fieldType,
                    name: f?.name || '',
                    description: f?.description,
                    required: f?.required,
                    editable: f?.editable,
                    unique: f?.unique,
                    options: f?.options?.nodes?.map((o) => ({ id: o?.id, name: o?.name })),
                  })),
                }}
                initialValues={{
                  locale: selectedLocale,
                  name: '',
                  description: '',
                  startAt: null,
                  finishAt: null,
                  values: layer.fields?.nodes?.map((field) => {
                    return {
                      layerFieldId: field?.id as string,
                      stringValue: undefined,
                      textValue: undefined,
                      integerValue: undefined,
                      floatValue: undefined,
                      booleanValue: undefined,
                      dateValue: null,
                      timeValue: null,
                      datetimeValue: null,
                      layerFieldOptionId: '0',
                    };
                  }),
                }}
                onLocaleChange={setSelectedLocale}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  createMarkerMutation({
                    variables: {
                      attributes: {
                        locale: values.locale,
                        layerId: layer.id || '',
                        name: values.name,
                        description: values.description,
                        avatar: values.avatar,
                        geocodingPoint: {
                          latitude: values.geocodingPoint?.latitude as number,
                          longitude: values.geocodingPoint?.longitude as number,
                        },
                        startAt: layer.timeInterval === 'disabled' || layer.startAtCurrentTime ? null : values.startAt,
                        finishAt: layer.timeInterval === 'any' ? values.finishAt : null,
                        values: values.values?.map((v) => ({
                          layerFieldId: v.layerFieldId,
                          stringValue: v.stringValue,
                          textValue: v.textValue,
                          integerValue: v.integerValue,
                          floatValue: v.floatValue,
                          booleanValue: v.booleanValue,
                          dateValue: v.dateValue ? fixDateLocale(v.dateValue) : undefined,
                          timeValue: v.timeValue,
                          datetimeValue: v.datetimeValue,
                          layerFieldOptionId: v.layerFieldOptionId !== '0' ? v.layerFieldOptionId : undefined,
                        })),
                        translate: values.translate,
                      },
                    },
                  })
                    .then((r) => {
                      const createMarker = r.data?.createMarker;

                      if (createMarker?.success) {
                        showSnackbar(createMarker.message || '', 'success');
                        router.push({
                          pathname: '/',
                          query: { layerId: router.query.layerId, markerId: createMarker.marker?.id },
                        });
                      } else {
                        showSnackbar(createMarker?.message || '', 'error');
                        const errors: Partial<FormikValues> = { data: [{ values: [] }] };
                        createMarker?.errors?.forEach((error: { path: string[]; message: string }) => {
                          switch (error.path[1]) {
                            case 'data':
                              if (error.path.length === 6 && !isNaN(Number(error.path[4]))) {
                                if (!errors.data[0].values[Number(error.path[4])]) {
                                  errors.data[0].values[Number(error.path[4])] = {};
                                }
                                errors.data[0].values[Number(error.path[4])][error.path[5]] = error.message;
                              }
                              break;
                            default:
                              errors[error.path[1]] = error.message;
                              break;
                          }
                        });
                        setErrors(errors);
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
                      setSubmitting(false);
                    });
                }}
              />
            );
          }
          return <></>;
        }}
      </LayerFieldsQueryComponent>
    </BaseLayout>
  );
};
