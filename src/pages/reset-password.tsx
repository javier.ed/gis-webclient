import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Formik, FormikValues, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { Button } from '@material-ui/core';
import { useResetPasswordMutationMutation } from '../graphql/mutations/ResetPasswordMutation.graphql';
import BaseLayout from '../layouts/base-layout';
import { AppContext } from './_app';

export default () => {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [resetPasswordMutation] = useResetPasswordMutationMutation();

  session.requireNoAuthentication(router);

  const goBack = () => {
    router.push('/login', '/login', { shallow: true });
  };

  return (
    <BaseLayout title={i18n.t('resetPassword')} onBackButtonClick={goBack} noindex nofollow>
      <Formik
        initialValues={{
          confirmationCode: '',
          password: '',
          passwordConfirmation: '',
        }}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (values.confirmationCode.trim().length === 0) {
            errors.confirmationCode = i18n.t('cantBeBlank');
          }
          if (values.password.trim().length === 0) {
            errors.password = i18n.t('cantBeBlank');
          }
          if (values.passwordConfirmation.trim().length === 0) {
            errors.passwordConfirmation = i18n.t('cantBeBlank');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          resetPasswordMutation({
            variables: {
              login: router.query.login?.toString() || '',
              confirmationCode: values.confirmationCode,
              attributes: {
                password: values.password,
                passwordConfirmation: values.passwordConfirmation,
              },
            },
          })
            .then((result) => {
              const resetPassword = result.data?.resetPassword;

              if (resetPassword?.success) {
                showSnackbar(resetPassword.message || '', 'success');
                router.push('/login', '/login', { shallow: true });
              } else {
                showSnackbar(resetPassword?.message || '', 'error');
                const errors: Partial<FormikValues> = {};
                resetPassword?.errors?.forEach((error: { path: string[]; message: string }) => {
                  errors[error.path[1]] = error.message;
                });
                setErrors(errors);
              }
              setSubmitting(false);
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest'), 'success');
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form autoComplete="off">
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="confirmationCode"
              label={i18n.t('confirmationCode')}
              type="password"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label={i18n.t('newPassword')}
              type="password"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="passwordConfirmation"
              label={i18n.t('passwordConfirmation')}
              type="password"
              autoComplete="off"
            />
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} fullWidth variant="contained" color="default">
              {i18n.t('resetPassword')}
            </Button>
          </Form>
        )}
      </Formik>
    </BaseLayout>
  );
};
