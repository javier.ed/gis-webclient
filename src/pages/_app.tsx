import React, { useState, createContext, useEffect } from 'react';
import { AppProps } from 'next/app';
import '../style.scss';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { Rosetta } from 'rosetta';
import { getDataFromTree } from '@apollo/client/react/ssr';
import { PageTransition } from 'next-page-transitions';
import { DefaultSeo } from 'next-seo';
import withApollo from '../utils/with-apollo';
import Session from '../utils/session';
import i18n, { languages } from '../utils/i18n';

interface AppContextProps {
  showSnackbar: (message: string, severity: 'info' | 'success' | 'warning' | 'error') => void;
  i18n: Rosetta<any>;
  session: Session;
  getCurrentPosition: (successCallback: (value: Position) => void) => void;
  locale: string;
}

let lang = 'en';

const session = new Session();

export const AppContext = createContext<AppContextProps>({
  showSnackbar: () => {},
  i18n: i18n(lang),
  session,
  getCurrentPosition: () => {},
  locale: 'en',
});

function BululuApp({
  Component,
  pageProps,
  acceptLanguage,
  router,
}: AppProps & { acceptLanguage: string | null | undefined }) {
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarSeverity, setSnackbarSeverity] = useState<'info' | 'success' | 'warning' | 'error' | undefined>();
  const [snackbarMessage, setSnackbarMessage] = useState<string | undefined>();

  const showSnackbar = (message: string, severity: 'info' | 'success' | 'warning' | 'error' = 'info') => {
    setSnackbarMessage(message);
    setSnackbarSeverity(severity);
    setOpenSnackbar(true);
  };

  const handleSnackbarClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnackbar(false);
  };

  const getCurrentPosition = (successCallback: (value: Position) => void) => {
    if (typeof navigator !== 'undefined' && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (value) => {
          successCallback(value);
        },
        (error) => showSnackbar(error.message, 'error'),
        { enableHighAccuracy: true, timeout: 60000, maximumAge: 86400000 },
      );
    }
  };

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    jssStyles?.parentElement?.removeChild(jssStyles);
  }, []);

  let clientLanguages: string[] = [];

  if (typeof window !== 'undefined') {
    clientLanguages = window.navigator.languages.map((l) => l.substr(0, 2));
  } else if (acceptLanguage) {
    clientLanguages = acceptLanguage.split(',').map((l) => l.substr(0, 2));
  }

  lang = clientLanguages.find((l) => languages.includes(l)) || 'en';

  return (
    <AppContext.Provider value={{ showSnackbar, i18n: i18n(lang), session, getCurrentPosition, locale: lang }}>
      <DefaultSeo
        additionalMetaTags={[{ name: 'application-name', content: 'Bululú' }]}
        openGraph={{ locale: lang, site_name: 'Bululú', type: 'website' }}
      />
      <PageTransition timeout={300} classNames="page-transition">
        <Component {...pageProps} key={router.route} />
      </PageTransition>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        autoHideDuration={3000}
        open={openSnackbar}
        onClose={handleSnackbarClose}
      >
        <Alert severity={snackbarSeverity} onClose={handleSnackbarClose}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </AppContext.Provider>
  );
}

BululuApp.getInitialProps = async (appContext: any) => {
  let pageProps = {};
  if (appContext?.Component?.getInitialProps) {
    pageProps = await appContext.Component.getInitialProps(appContext?.ctx);
  }
  return { pageProps, acceptLanguage: appContext?.ctx?.req?.headers['accept-language'] };
};

export default withApollo(BululuApp, { getDataFromTree });
