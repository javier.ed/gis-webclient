import { useContext } from 'react';
import { useRouter } from 'next/router';
import { Card, CardActionArea, CardContent, Typography, Grid } from '@material-ui/core';
import Link from 'next/link';
import { CurrentUserQueryComponent } from '../../graphql/queries/CurrentUserQuery.graphql';
import { AppContext } from '../_app';
import BaseLayout from '../../layouts/base-layout';

export default () => {
  const { session, i18n } = useContext(AppContext);
  const router = useRouter();

  session.requireAuthentication(router);

  return (
    <BaseLayout title={i18n.t('settings')} noindex nofollow>
      <CurrentUserQueryComponent>
        {(result) => (
          <>
            <Link href="/settings/profile" passHref>
              <a>
                <Card className="item-card">
                  <CardActionArea>
                    <CardContent>
                      <Typography className="item-card-title">{i18n.t('editProfile')}</Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </a>
            </Link>
            <Link href="/settings/email" passHref>
              <a>
                <Card className="item-card">
                  <CardActionArea>
                    <CardContent>
                      <Typography className="item-card-title">{i18n.t('email')}</Typography>
                      <Grid container spacing={1}>
                        <Grid item>
                          <Typography>{result.data?.currentUser?.email}</Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            color="error"
                            style={
                              result.loading || result.data?.currentUser?.emailVerified
                                ? { display: 'none' }
                                : undefined
                            }
                          >
                            ({i18n.t('unverified')})
                          </Typography>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </a>
            </Link>
            <Link href="/settings/password" passHref>
              <a>
                <Card className="item-card">
                  <CardActionArea>
                    <CardContent>
                      <Typography className="item-card-title">{i18n.t('changePassword')}</Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </a>
            </Link>
          </>
        )}
      </CurrentUserQueryComponent>
    </BaseLayout>
  );
};
