import { useRouter } from 'next/router';
import { useContext, useReducer } from 'react';
import { Formik, Form, Field, FormikValues } from 'formik';
import { TextField, Select } from 'formik-material-ui';
import {
  Button,
  InputAdornment,
  IconButton,
  FormControl,
  InputLabel,
  MenuItem,
  CircularProgress,
} from '@material-ui/core';
import { DatePicker } from 'formik-material-ui-pickers';
import { Event } from '@material-ui/icons';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import BaseLayout from '../../layouts/base-layout';
import ResponsiveBreadcrumbs from '../../components/responsive-breadcrumbs';
import { AppContext } from '../_app';
import { useUpdateProfileMutationMutation } from '../../graphql/mutations/UpdateProfileMutation.graphql';
import { EditCurrentUserProfileQueryComponent } from '../../graphql/queries/EditCurrentUserProfileQuery.graphql';
import { AllCountriesQueryComponent } from '../../graphql/queries/AllCountriesQuery.graphql';
import ImageField, { allowedImageTypes } from '../../components/image-field';
import { dateParse, fixDateLocale } from '../../utils/date-helpers';

export default () => {
  const { session, i18n, showSnackbar } = useContext(AppContext);
  const router = useRouter();
  const [, forceUpdate] = useReducer((x) => x + 1, 0);

  session.requireAuthentication(router);

  const [updateProfileMutation] = useUpdateProfileMutationMutation();

  const appBarTitle = () => {
    return (
      <ResponsiveBreadcrumbs
        items={[
          { label: i18n.t('settings'), href: '/settings', className: 'app-bar-title' },
          { label: i18n.t('editProfile'), className: 'app-bar-title' },
        ]}
      />
    );
  };

  return (
    <BaseLayout title={`${i18n.t('settings')} > ${i18n.t('editProfile')}`} appBarTitle={appBarTitle()} noindex nofollow>
      <EditCurrentUserProfileQueryComponent fetchPolicy="network-only">
        {(result) => {
          const currentUser = result.data?.currentUser;
          if (currentUser) {
            const profile = currentUser.profile;
            return (
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Formik
                  initialValues={{
                    name: profile?.name,
                    bio: profile?.bio,
                    countryId: profile?.countryId || '0',
                    birthdate: profile?.birthdate ? dateParse(profile.birthdate) : null,
                    avatar: undefined as any,
                    avatarUrl: profile?.avatarUrl,
                    removeAvatar: false,
                  }}
                  validate={(values) => {
                    const errors: Partial<FormikValues> = {};
                    if (values.avatar && !allowedImageTypes.includes(values.avatar.type)) {
                      errors.avatar = i18n.t('isInvalid');
                    }
                    return errors;
                  }}
                  onSubmit={(values, { setSubmitting, setErrors }) => {
                    updateProfileMutation({
                      variables: {
                        attributes: {
                          name: values.name,
                          bio: values.bio,
                          countryId: values.countryId === '0' ? '' : values.countryId,
                          birthdate: values.birthdate ? fixDateLocale(values.birthdate) : undefined,
                          avatar: values.avatar,
                          removeAvatar: values.removeAvatar,
                        },
                      },
                    })
                      .then((r) => {
                        const updateProfile = r.data?.updateProfile;

                        if (updateProfile?.success) {
                          showSnackbar(updateProfile.message || '', 'success');
                          router.push('/settings', '/settings', { shallow: true });
                        } else {
                          showSnackbar(updateProfile?.message || '', 'error');
                          const errors: Partial<FormikValues> = {};
                          updateProfile?.errors?.forEach((error: { path: string[]; message: string }) => {
                            errors[error.path[1]] = error.message;
                          });
                          setErrors(errors);
                          setSubmitting(false);
                        }
                      })
                      .catch(() => {
                        showSnackbar(i18n.t('failedToExecuteRequest') || '', 'error');
                        setSubmitting(false);
                      });
                  }}
                >
                  {({ submitForm, isSubmitting, values, errors }) => (
                    <Form autoComplete="off">
                      <Field
                        component={TextField}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="name"
                        label={i18n.t('name')}
                      />
                      <Field
                        component={TextField}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="bio"
                        label={i18n.t('bio')}
                        multiline
                        rows={2}
                        rowsMax={4}
                      />
                      <AllCountriesQueryComponent>
                        {(r) => (
                          <FormControl variant="outlined" margin="normal" fullWidth>
                            <InputLabel htmlFor="countryId">{i18n.t('country')}</InputLabel>
                            <Field
                              component={Select}
                              variant="outlined"
                              name="countryId"
                              inputProps={{ id: 'countryId' }}
                              label={i18n.t('country')}
                            >
                              <MenuItem value="0">{i18n.t('none')}</MenuItem>
                              {r.data?.allCountries?.map((country) => (
                                <MenuItem key={country?.id} value={country?.id}>
                                  {country?.name}
                                </MenuItem>
                              ))}
                            </Field>
                          </FormControl>
                        )}
                      </AllCountriesQueryComponent>
                      <Field
                        component={DatePicker}
                        name="birthdate"
                        label={i18n.t('birthdate')}
                        fullWidth
                        inputVariant="outlined"
                        margin="normal"
                        clearable
                        format="yyyy-MM-dd"
                        disableFuture
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton>
                                <Event />
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <ImageField
                        label={i18n.t('avatar')}
                        imageUrl={values.avatarUrl}
                        disabled={isSubmitting}
                        error={typeof errors.avatar === 'string' ? errors.avatar : undefined}
                        onChangeImage={(image) => {
                          if (image) {
                            /* eslint-disable no-param-reassign */
                            values.avatar = image;
                          } else if (values.avatarUrl) {
                            values.avatarUrl = undefined;
                            values.removeAvatar = true;
                          } else {
                            values.avatar = undefined;
                            /* eslint-enable no-param-reassign */
                          }
                          forceUpdate();
                        }}
                      />
                      <br />
                      <br />
                      <Button
                        disabled={isSubmitting}
                        onClick={submitForm}
                        variant="contained"
                        color="default"
                        fullWidth
                      >
                        {i18n.t('submit')}
                      </Button>
                      <br />
                      <br />
                    </Form>
                  )}
                </Formik>
              </MuiPickersUtilsProvider>
            );
          }
          return (
            <div style={{ display: 'flex' }}>
              <CircularProgress style={{ margin: '8px auto' }} />
            </div>
          );
        }}
      </EditCurrentUserProfileQueryComponent>
    </BaseLayout>
  );
};
