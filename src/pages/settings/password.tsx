import { useContext } from 'react';
import { useRouter } from 'next/router';
import { Formik, FormikValues, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { Button } from '@material-ui/core';
import ResponsiveBreadcrumbs from '../../components/responsive-breadcrumbs';
import BaseLayout from '../../layouts/base-layout';
import { AppContext } from '../_app';
import { useChangePasswordMutationMutation } from '../../graphql/mutations/ChangePasswordMutation.graphql';

export default () => {
  const { session, i18n, showSnackbar } = useContext(AppContext);
  const router = useRouter();

  session.requireAuthentication(router);

  const [changePasswordMutation] = useChangePasswordMutationMutation();

  const appBarTitle = () => {
    return (
      <ResponsiveBreadcrumbs
        items={[
          { label: i18n.t('settings'), href: '/settings', className: 'app-bar-title' },
          { label: i18n.t('changePassword'), className: 'app-bar-title' },
        ]}
      />
    );
  };

  return (
    <BaseLayout
      title={`${i18n.t('settings')} > ${i18n.t('changePassword')}`}
      appBarTitle={appBarTitle()}
      noindex
      nofollow
    >
      <Formik
        initialValues={{
          password: '',
          attributes: { password: '', passwordConfirmation: '' },
        }}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (values.password.trim().length === 0) {
            errors.password = i18n.t('cantBeBlank');
          }
          if (values.attributes.password.trim().length === 0) {
            if (typeof errors.attributes === 'undefined') {
              errors.attributes = {};
            }
            errors.attributes.password = i18n.t('cantBeBlank');
          }
          if (values.attributes.passwordConfirmation.trim().length === 0) {
            if (typeof errors.attributes === 'undefined') {
              errors.attributes = {};
            }
            errors.attributes.passwordConfirmation = i18n.t('cantBeBlank');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          changePasswordMutation({ variables: values })
            .then((result) => {
              const changePassword = result.data?.changePassword;

              if (changePassword?.success) {
                showSnackbar(changePassword.message || '', 'success');
                router.push('/settings', '/settings', { shallow: true });
              } else {
                showSnackbar(changePassword?.message || '', 'error');
                const errors: Partial<FormikValues> = { attributes: {} };
                changePassword?.errors?.forEach((error: { path: string[]; message: string }) => {
                  switch (error.path[0]) {
                    case 'password':
                      errors.password = error.message;
                      break;
                    case 'attributes':
                      errors.attributes[error.path[1]] = error.message;
                      break;
                    default:
                  }
                });
                setErrors(errors);
                setSubmitting(false);
              }
            })
            .catch(() => {
              showSnackbar(i18n.t('failedToExecuteRequest') || '', 'error');
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form autoComplete="off">
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label={i18n.t('currentPassword')}
              type="password"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="attributes.password"
              label={i18n.t('newPassword')}
              type="password"
              autoComplete="off"
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="attributes.passwordConfirmation"
              label={i18n.t('passwordConfirmation')}
              type="password"
              autoComplete="off"
            />
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} variant="contained" color="default" fullWidth>
              {i18n.t('changePassword')}
            </Button>
            <br />
            <br />
          </Form>
        )}
      </Formik>
    </BaseLayout>
  );
};
