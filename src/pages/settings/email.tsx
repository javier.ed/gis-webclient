import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { Grid, Typography, Button, Dialog, DialogContent, DialogTitle, DialogActions } from '@material-ui/core';
import { Formik, Form, Field, FormikValues } from 'formik';
import { TextField } from 'formik-material-ui';
import { CurrentUserQueryComponent } from '../../graphql/queries/CurrentUserQuery.graphql';
import { AppContext } from '../_app';
import BaseLayout from '../../layouts/base-layout';
import { useVerifyEmailMutationMutation } from '../../graphql/mutations/VerifyEmailMutation.graphql';
import ResponsiveBreadcrumbs from '../../components/responsive-breadcrumbs';
import { useChangeEmailMutationMutation } from '../../graphql/mutations/ChangeEmailMutation.graphql';

export default () => {
  const { session, i18n, showSnackbar } = useContext(AppContext);
  const router = useRouter();
  const [openChangeEmailDialog, setOpenChangeEmailDialog] = useState(false);

  session.requireAuthentication(router);

  const [verifyEmailMutation] = useVerifyEmailMutationMutation();
  const [changeEmailMutation] = useChangeEmailMutationMutation();

  const appBarTitle = () => {
    return (
      <ResponsiveBreadcrumbs
        items={[
          { label: i18n.t('settings'), href: '/settings', className: 'app-bar-title' },
          { label: i18n.t('email'), className: 'app-bar-title' },
        ]}
      />
    );
  };

  return (
    <BaseLayout title={`${i18n.t('settings')} > ${i18n.t('email')}`} appBarTitle={appBarTitle()} noindex nofollow>
      <CurrentUserQueryComponent>
        {(result) => (
          <>
            <br />
            <Grid container spacing={1} alignItems="center">
              <Grid item>
                <Typography style={{ fontWeight: 'bold', fontSize: 'large' }} color="textSecondary">
                  {result.data?.currentUser?.email}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  color="error"
                  style={result.loading || result.data?.currentUser?.emailVerified ? { display: 'none' } : undefined}
                >
                  ({i18n.t('unverified')})
                </Typography>
              </Grid>
              <Grid item>
                <Button onClick={() => setOpenChangeEmailDialog(true)}>{i18n.t('change')}</Button>
              </Grid>
            </Grid>
            <Dialog
              open={openChangeEmailDialog}
              onClose={() => setOpenChangeEmailDialog(false)}
              disableEscapeKeyDown
              disableBackdropClick
            >
              <DialogTitle>{i18n.t('changeEmail')}</DialogTitle>
              <Formik
                initialValues={{ email: '', password: '' }}
                validate={(values) => {
                  const errors: Partial<FormikValues> = {};
                  if (values.email.trim().length === 0) {
                    errors.email = i18n.t('cantBeBlank');
                  }
                  if (values.password.trim().length === 0) {
                    errors.password = i18n.t('cantBeBlank');
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  changeEmailMutation({ variables: values })
                    .then((r) => {
                      const changeEmail = r.data?.changeEmail;

                      if (changeEmail?.success) {
                        showSnackbar(changeEmail.message || '', 'success');
                        router.push('/settings', '/settings', { shallow: true });
                      } else {
                        showSnackbar(changeEmail?.message || '', 'error');
                        const errors: Partial<FormikValues> = {};
                        changeEmail?.errors?.forEach((error: { path: string[]; message: string }) => {
                          errors[error.path[0]] = error.message;
                        });
                        setErrors(errors);
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest') || '', 'error');
                      setSubmitting(false);
                    });
                }}
              >
                {({ submitForm, isSubmitting }) => (
                  <Form autoComplete="off">
                    <DialogContent>
                      <Field
                        component={TextField}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label={i18n.t('password')}
                        type="password"
                      />
                      <Field
                        component={TextField}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="email"
                        label={i18n.t('email')}
                        type="email"
                      />
                    </DialogContent>
                    <DialogActions>
                      <Button disabled={isSubmitting} onClick={() => setOpenChangeEmailDialog(false)} color="primary">
                        {i18n.t('cancel')}
                      </Button>
                      <Button disabled={isSubmitting} onClick={submitForm} color="primary">
                        {i18n.t('submit')}
                      </Button>
                    </DialogActions>
                  </Form>
                )}
              </Formik>
            </Dialog>
            <br />
            <div style={result.loading || result.data?.currentUser?.emailVerified ? { display: 'none' } : undefined}>
              <Formik
                initialValues={{ confirmationCode: '' }}
                validate={(values) => {
                  const errors: Partial<FormikValues> = {};
                  if (values.confirmationCode.trim().length === 0) {
                    errors.confirmationCode = i18n.t('cantBeBlank');
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  verifyEmailMutation({ variables: { confirmationCode: values.confirmationCode } })
                    .then((r) => {
                      const verifyEmail = r.data?.verifyEmail;

                      if (verifyEmail?.success) {
                        showSnackbar(verifyEmail.message || '', 'success');
                        router.push('/settings', '/settings', { shallow: true });
                      } else {
                        showSnackbar(verifyEmail?.message || '', 'error');
                      }
                      setSubmitting(false);
                    })
                    .catch(() => {
                      showSnackbar(i18n.t('failedToExecuteRequest') || '', 'error');
                      setSubmitting(false);
                    });
                }}
              >
                {({ submitForm, isSubmitting }) => (
                  <Form autoComplete="off">
                    <Field
                      component={TextField}
                      variant="outlined"
                      margin="normal"
                      required
                      fullWidth
                      label={i18n.t('confirmationCode')}
                      name="confirmationCode"
                    />
                    <Button disabled={isSubmitting} onClick={submitForm} fullWidth variant="contained" color="default">
                      {i18n.t('verifyEmailAddress')}
                    </Button>
                    <br />
                    <br />
                  </Form>
                )}
              </Formik>
            </div>
          </>
        )}
      </CurrentUserQueryComponent>
    </BaseLayout>
  );
};
