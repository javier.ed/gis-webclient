import { ApolloClient, InMemoryCache, NormalizedCacheObject } from '@apollo/client';
import { persistCache, PersistentStorage } from 'apollo3-cache-persist';
import { PersistedData } from 'apollo3-cache-persist/lib/types';
import { createUploadLink } from 'apollo-upload-client';
import settings from './settings';
// eslint-disable-next-line import/no-cycle
import Session from './session';

interface HeadersProps {
  'X-Session-Id'?: string;
  'X-Session-Token'?: string;
  'X-Client-App-Id'?: string;
  'X-Client-App-Token'?: string;
  'Accept-Language'?: string;
}

let apolloClientInstance: ApolloClient<NormalizedCacheObject> | undefined;

const cache = new InMemoryCache();

if (typeof window !== 'undefined') {
  persistCache({ cache, storage: window.localStorage as PersistentStorage<PersistedData<NormalizedCacheObject>> });
}

export const createApolloClient = (headers?: HeadersProps) => {
  const session = new Session();
  let defaultHeaders: HeadersProps = {};

  if (session.exists()) {
    defaultHeaders = {
      'X-Session-Id': session.getId(),
      'X-Session-Token': session.getToken({ clientAppId: settings.getAppId(), userId: session.getUserId() }),
    };
  } else {
    defaultHeaders = { 'X-Client-App-Id': settings.getAppId(), 'X-Client-App-Token': settings.getAppToken() };
  }

  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: createUploadLink({ uri: settings.getServerUrl(), headers: { ...defaultHeaders, ...headers } }),
    cache,
    defaultOptions: {
      query: { fetchPolicy: 'network-only' },
      watchQuery: { fetchPolicy: 'cache-and-network' },
      mutate: { fetchPolicy: 'no-cache' },
    },
  });
};

export function clearApolloClient(): void {
  apolloClientInstance = undefined;
  if (typeof window !== 'undefined') {
    window.localStorage.removeItem('apollo-cache-persist');
  }
}

export default function apolloClient(
  initialState: any = null,
  headers: any = null,
): ApolloClient<NormalizedCacheObject> {
  if (!apolloClientInstance) {
    if (typeof window !== 'undefined') {
      apolloClientInstance = createApolloClient();
    } else {
      apolloClientInstance = createApolloClient({ 'Accept-Language': headers['accept-language'] });
    }
  }

  if (initialState) {
    apolloClientInstance.cache.restore(initialState);
  }

  return apolloClientInstance;
}
