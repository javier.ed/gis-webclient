export function dateParse(source: string) {
  return new Date(Date.parse(`${source}T00:00`));
}

export function fixDateLocale(source: Date) {
  return new Date(Date.UTC(source.getFullYear(), source.getMonth(), source.getDate()));
}
