import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ddd',
    },
  },
  overrides: {
    MuiButton: {
      textPrimary: {
        color: '#555',
      },
    },
  },
});

export default theme;
