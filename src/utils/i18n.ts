import rosetta, { Rosetta } from 'rosetta';
import en from '../locales/en.json';
import es from '../locales/es.json';

const i18n = rosetta({ en, es });

export const languages = ['en', 'es'];

export default (lang = 'en'): Rosetta<any> => {
  i18n.locale(lang);
  return i18n;
};
