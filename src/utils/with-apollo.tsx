import React from 'react';
import withApollo from 'next-with-apollo';
import { ApolloProvider } from '@apollo/client';
import apolloClient from './apollo-client';

export default withApollo(({ initialState, headers }) => apolloClient(initialState, headers), {
  render: function nextWithApollo({ Page, props }) {
    return (
      <ApolloProvider client={props.apollo}>
        <Page {...props} />
      </ApolloProvider>
    );
  },
});
