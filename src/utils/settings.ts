import { sign, verify } from 'jsonwebtoken';

export default new (class Settings {
  private settings = {
    appId: process.env.NEXT_PUBLIC_APP_ID || '',
    appKey: process.env.NEXT_PUBLIC_APP_KEY || '',
    serverUrl: process.env.NEXT_PUBLIC_SERVER_URL || 'https://core.bululu.ml:4330/graphql',
    websocketUrl: process.env.NEXT_PUBLIC_WEBSOCKET_URL || 'wss://core.bululu.ml:4330/cable',
    disableRegister: process.env.NEXT_PUBLIC_DISABLE_REGISTER || '0',
  };

  getAppId() {
    return this.settings.appId;
  }

  getServerUrl() {
    return this.settings.serverUrl;
  }

  getWebsocketUrl() {
    return this.settings.websocketUrl;
  }

  getAppToken(payload = {}) {
    return sign(payload, this.settings.appKey);
  }

  decodeAppToken(token: string) {
    return verify(token, this.settings.appKey);
  }

  registerDisabled(): boolean {
    return this.settings.disableRegister === '1';
  }
})();
