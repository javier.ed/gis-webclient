import { sign } from 'jsonwebtoken';
import Router, { NextRouter } from 'next/router';
// eslint-disable-next-line import/no-cycle
import { clearApolloClient } from './apollo-client';

export default class Session {
  exists(): boolean {
    return typeof window !== 'undefined' && window.localStorage.getItem('current_session') != null;
  }

  start(id: string, key: string, userId: string, username: string): void {
    if (typeof window !== 'undefined') {
      window.localStorage.setItem('current_session', JSON.stringify({ id, key, userId, username }));
      clearApolloClient();
    }
  }

  finish(): void {
    if (typeof window !== 'undefined') {
      window.localStorage.removeItem('current_session');
      clearApolloClient();
    }
  }

  getId(): string | undefined {
    if (typeof window !== 'undefined') {
      return JSON.parse(window.localStorage.getItem('current_session') || '{}').id.toString();
    }
    return undefined;
  }

  getUserId(): string | undefined {
    if (typeof window !== 'undefined') {
      return JSON.parse(window.localStorage.getItem('current_session') || '{}').userId.toString();
    }
    return undefined;
  }

  getUsername(): string | undefined {
    if (typeof window !== 'undefined') {
      return JSON.parse(window.localStorage.getItem('current_session') || '{}').username;
    }
    return undefined;
  }

  getToken(payload = {}): string | undefined {
    if (typeof window !== 'undefined') {
      return sign(payload, JSON.parse(window.localStorage.getItem('current_session') || '{}').key);
    }
    return undefined;
  }

  requireAuthentication(router?: NextRouter | null): void {
    if (typeof window !== 'undefined' && !this.exists()) {
      (router || Router).push('/login', '/login', { shallow: true });
    }
  }

  requireNoAuthentication(router?: NextRouter): void {
    if (typeof window !== 'undefined' && this.exists()) {
      (router || Router).push('/', '/', { shallow: true });
    }
  }
}
