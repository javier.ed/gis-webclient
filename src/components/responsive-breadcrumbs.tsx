import { Breadcrumbs, Typography } from '@material-ui/core';
import Link from 'next/link';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import theme from '../utils/theme';

interface ItemProps {
  label: string;
  href?: string;
  as?: string;
  className?: string;
}

export default function ResponsiveBreadcrumbs(props: { items: ItemProps[] }) {
  const matches = useMediaQuery(theme.breakpoints.up('md'));

  const showItem = (item: ItemProps, index?: number) => {
    if (item.href) {
      return (
        <Link key={index} href={item.href} as={item.as} passHref>
          <a className={item.className}>{item.label}</a>
        </Link>
      );
    }
    return (
      <Typography key={index} className={item.className} color="textPrimary">
        {item.label}
      </Typography>
    );
  };

  if (matches) {
    return (
      <Breadcrumbs separator=">" aria-label="breadcrumb">
        {props.items.map((item, index) => showItem(item, index))}
      </Breadcrumbs>
    );
  }
  const item = props.items[props.items.length - 1];
  return showItem(item);
}
