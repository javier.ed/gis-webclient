import { Button, Collapse, Grid, Typography, Theme, makeStyles, createStyles } from '@material-ui/core';
import { useState, useContext, ReactNode } from 'react';
import { ExpandMore } from '@material-ui/icons';
import { AppContext } from '../pages/_app';

export default function Collapsible(props: { children: ReactNode }) {
  const { i18n } = useContext(AppContext);
  const [expanded, setExpanded] = useState(false);

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
    }),
  );
  const classes = useStyles();

  return (
    <>
      <Button
        onClick={() => setExpanded(!expanded)}
        aria-expanded={expanded}
        aria-label={expanded ? i18n.t('showLess') : i18n.t('showMore')}
        fullWidth
        size="small"
      >
        <Grid container alignItems="center">
          <Grid item xs>
            <Typography color="textSecondary">{expanded ? i18n.t('showLess') : i18n.t('showMore')}</Typography>
          </Grid>
          <Grid item>
            <div className={expanded ? `${classes.expand} ${classes.expandOpen}` : classes.expand}>
              <ExpandMore />
            </div>
          </Grid>
        </Grid>
      </Button>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {props.children}
      </Collapse>
    </>
  );
}
