import { useContext, useState } from 'react';
import { IconButton, Avatar, Dialog, DialogContent, Typography, Button, Menu, MenuItem } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useLogoutMutationMutation } from '../graphql/mutations/LogoutMutation.graphql';
import { AppContext } from '../pages/_app';
import Info from './info';
import { CurrentUserProfileAvatarQueryComponent } from '../graphql/queries/CurrentUserProfileAvatarQuery.graphql';
import { CurrentUserProfileQueryComponent } from '../graphql/queries/CurrentUserProfileQuery.graphql';

export default function SessionMenuItem() {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [openUserDialog, setOpenUserDialog] = useState(false);
  const [logoutMutation] = useLogoutMutationMutation();
  const [openInfoDialog, setOpenInfoDialog] = useState(false);
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null);

  if (typeof window !== 'undefined') {
    if (session.exists()) {
      const attemptToLogout = () => {
        setOpenUserDialog(false);
        showSnackbar(i18n.t('loggingOut'), 'info');
        logoutMutation()
          .then((result) => {
            const logout = result.data?.logout;
            if (logout?.success) {
              showSnackbar(logout.message || '', 'success');
              session.finish();
              window.location.replace('/');
            } else {
              showSnackbar(logout?.message || '', 'error');
            }
          })
          .catch(() => showSnackbar(i18n.t('failedToExecuteRequest'), 'error'));
      };

      const avatarUrl = `https://robohash.org/${session.getUsername()}?set=set4&bgset=bg1`;

      return (
        <>
          <IconButton onClick={() => setOpenUserDialog(true)}>
            <CurrentUserProfileAvatarQueryComponent>
              {(result) => (
                <Avatar src={result.data?.currentUser?.profile?.avatarUrl || avatarUrl}>
                  @{result.data?.currentUser?.username || session.getUsername()?.[0]?.toUpperCase()}
                </Avatar>
              )}
            </CurrentUserProfileAvatarQueryComponent>
          </IconButton>

          <Dialog onClose={() => setOpenUserDialog(false)} open={openUserDialog} fullWidth>
            <DialogContent>
              <CurrentUserProfileQueryComponent>
                {(result) => (
                  <>
                    <Avatar
                      className="user-dialog-avatar"
                      src={result.data?.currentUser?.profile?.avatarUrl || avatarUrl}
                    >
                      @{result.data?.currentUser?.username || session.getUsername()?.[0]?.toUpperCase()}
                    </Avatar>

                    <Typography component="h3" variant="h3" className="dialog-title">
                      {result.data?.currentUser?.profile?.name}
                    </Typography>
                    <Typography className="dialog-description" style={{ textAlign: 'center' }}>
                      @{result.data?.currentUser?.username || session.getUsername()?.[0]?.toUpperCase()}
                    </Typography>
                  </>
                )}
              </CurrentUserProfileQueryComponent>

              <br />

              <Link href="/your-layers" passHref>
                <Button variant="outlined" fullWidth>
                  {i18n.t('yourLayers')}
                </Button>
              </Link>

              <br />
              <br />

              <Link href="/your-markers" passHref>
                <Button variant="outlined" fullWidth>
                  {i18n.t('yourMarkers')}
                </Button>
              </Link>

              <br />
              <br />

              <Link href="/settings" passHref>
                <Button variant="outlined" fullWidth>
                  {i18n.t('settings')}
                </Button>
              </Link>

              <br />
              <br />

              <Button variant="outlined" fullWidth onClick={attemptToLogout}>
                {i18n.t('logout')}
              </Button>

              <br />
              <br />

              <Info />
            </DialogContent>
          </Dialog>
        </>
      );
    }
    const goToLogin = () => {
      router.push('/login', '/login', { shallow: true });
      setAnchorMenu(null);
    };

    const goToInfoDialog = () => {
      setOpenInfoDialog(true);
      setAnchorMenu(null);
    };

    return (
      <div>
        <IconButton onClick={(event) => setAnchorMenu(event.currentTarget)}>
          <MoreVert />
        </IconButton>
        <Menu anchorEl={anchorMenu} open={Boolean(anchorMenu)} onClose={() => setAnchorMenu(null)}>
          <MenuItem onClick={goToLogin}>{i18n.t('login')}</MenuItem>
          <MenuItem onClick={goToInfoDialog}>Info</MenuItem>
        </Menu>
        <Dialog onClose={() => setOpenInfoDialog(false)} open={openInfoDialog} fullWidth>
          <DialogContent>
            <Info />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
  return <></>;
}
