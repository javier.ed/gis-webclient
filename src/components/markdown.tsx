import ReactMarkdown, { NodeType } from 'react-markdown';

export default function Markdown(props: { className?: string; disallowedTypes?: NodeType[]; children: string }) {
  const disallowedTypes: NodeType[] = ['heading', 'html', 'image', 'imageReference', 'thematicBreak', 'virtualHtml'];
  return (
    <ReactMarkdown
      className={props.className}
      disallowedTypes={disallowedTypes.concat(props.disallowedTypes || [])}
      unwrapDisallowed
      linkTarget="_blank"
    >
      {props.children.replace(/\n/g, '  \n')}
    </ReactMarkdown>
  );
}
