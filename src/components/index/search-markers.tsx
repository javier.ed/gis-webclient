import {
  Typography,
  Drawer,
  Toolbar,
  IconButton,
  CircularProgress,
  Card,
  CardActionArea,
  CardContent,
  makeStyles,
  createStyles,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useContext } from 'react';
import { SearchMarkersQueryComponent, Marker } from '../../graphql/queries/SearchMarkersQuery.graphql';
import { AppContext } from '../../pages/_app';

export default function SearchMarkers(props: {
  query: string;
  openDrawer: boolean;
  onCloseDrawer: () => void;
  onMarkerClick: (marker: Marker) => void;
}) {
  const { i18n } = useContext(AppContext);

  const drawerWidth = 360;

  const useStyles = makeStyles(() =>
    createStyles({
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
      },
      drawerPaper: {
        width: drawerWidth,
      },
      drawerContainer: {
        overflow: 'auto',
        padding: '8px',
      },
    }),
  );
  const classes = useStyles();

  const showTimeRange = (marker?: Marker | null) => {
    if (marker?.startAt || marker?.finishAt) {
      return (
        <>
          <Typography>
            <span style={marker.startAt ? { marginRight: '12px' } : { display: 'none' }}>
              {i18n.t('startAt')} {new Date(marker.startAt).toLocaleString()}
            </span>
            <span style={marker.finishAt ? undefined : { display: 'none' }}>
              {i18n.t('finishAt')} {new Date(marker.finishAt).toLocaleString()}
            </span>
          </Typography>
        </>
      );
    }
    return <></>;
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      open={props.openDrawer}
      classes={{
        paper: classes.drawerPaper,
      }}
      anchor="right"
    >
      <Toolbar />

      <div style={{ textAlign: 'right' }}>
        <IconButton onClick={props.onCloseDrawer}>
          <Close />
        </IconButton>
      </div>

      <div id="drawerContainer" className={classes.drawerContainer}>
        <SearchMarkersQueryComponent
          variables={{ query: props.query.trim() }}
          skip={!props.openDrawer || props.query.trim().length < 2}
        >
          {(result) => (
            <InfiniteScroll
              dataLength={result.data?.searchMarkers?.nodes?.length || 0}
              hasMore={result.loading || result.data?.searchMarkers?.pageInfo.hasNextPage || false}
              next={() =>
                result.fetchMore({
                  variables: {
                    after: result.data?.searchMarkers?.pageInfo?.endCursor,
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    // eslint-disable-next-line prefer-object-spread
                    const newResult = Object.assign({}, fetchMoreResult);
                    if (newResult?.searchMarkers?.nodes) {
                      newResult.searchMarkers.nodes = [
                        ...(prev.searchMarkers?.nodes || []),
                        ...(fetchMoreResult?.searchMarkers?.nodes || []),
                      ];
                    }
                    return newResult;
                  },
                })
              }
              loader={
                <div style={{ display: 'flex' }}>
                  <CircularProgress style={{ margin: '8px auto' }} />
                </div>
              }
              scrollableTarget="drawerContainer"
            >
              {result.data?.searchMarkers?.nodes?.map((marker) => (
                <Card key={marker?.id} className="item-card" onClick={() => props.onMarkerClick(marker as Marker)}>
                  <CardActionArea>
                    <CardContent>
                      <Typography className="item-card-title">{marker?.name}</Typography>
                      <Typography>
                        {i18n.t('layer')}: <strong>{marker?.layer?.name}</strong>
                      </Typography>
                      {showTimeRange(marker)}
                    </CardContent>
                  </CardActionArea>
                </Card>
              ))}
            </InfiniteScroll>
          )}
        </SearchMarkersQueryComponent>
      </div>
    </Drawer>
  );
}
