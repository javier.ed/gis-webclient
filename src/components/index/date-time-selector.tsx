import { Button, ButtonGroup, useMediaQuery } from '@material-ui/core';
import { Event, KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { useState, useContext, useEffect } from 'react';
import { AppContext } from '../../pages/_app';
import theme from '../../utils/theme';

export default function DateTimeSelector(props: {
  value: Date | null;
  onChange: (date: Date | null) => void;
  hidden?: boolean;
  disabled?: boolean;
}) {
  const { showSnackbar, i18n } = useContext(AppContext);
  const [openDatetimePicker, setOpenDatetimePicker] = useState(false);
  const matchesSmDown = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    if (props.value) {
      showSnackbar(i18n.t('dateSelected', { value: props.value.toLocaleString() }), 'info');
    }
  }, [props.value]);

  const onClickArrowLeft = () => {
    const newDate = props.value ? new Date(props.value) : new Date();
    newDate.setDate(newDate.getDate() - 1);
    props.onChange(newDate);
  };

  const onClickArrowRight = () => {
    const newDate = props.value ? new Date(props.value) : new Date();
    newDate.setDate(newDate.getDate() + 1);
    props.onChange(newDate);
  };

  return (
    <div className="date-time-selector-container" hidden={props.hidden}>
      <ButtonGroup variant="outlined" orientation={matchesSmDown ? 'vertical' : 'horizontal'}>
        <Button onClick={onClickArrowLeft} disabled={props.disabled}>
          <KeyboardArrowLeft />
        </Button>
        <Button onClick={() => setOpenDatetimePicker(true)} disabled={props.disabled}>
          <span style={matchesSmDown ? { display: 'none' } : { marginRight: '1em' }}>
            {props.value?.toLocaleString() || i18n.t('now')}
          </span>
          <Event />
        </Button>
        <Button onClick={onClickArrowRight} disabled={props.disabled}>
          <KeyboardArrowRight />
        </Button>
      </ButtonGroup>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <DateTimePicker
          open={openDatetimePicker}
          onClose={() => setOpenDatetimePicker(false)}
          style={{ display: 'none' }}
          value={props.value}
          onChange={props.onChange}
          clearable
          format="yyyy-MM-dd HH:mm"
          ampm={false}
          minDate="1000-01-01"
        />
      </MuiPickersUtilsProvider>
    </div>
  );
}
