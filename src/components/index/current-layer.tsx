import { Chip, Dialog, DialogContent, Typography, Button, IconButton, Menu, MenuItem } from '@material-ui/core';
import { Layers, MoreVert, Star } from '@material-ui/icons';
import { useState, useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import { Layer, LayerQueryComponent } from '../../graphql/queries/LayerQuery.graphql';
import { AppContext } from '../../pages/_app';
import { useUnsubscribeFromLayerMutationMutation } from '../../graphql/mutations/UnsubscribeFromLayerMutation.graphql';
import { useSubscribeToLayerMutationMutation } from '../../graphql/mutations/SubscribeToLayerMutation.graphql';
import Markdown from '../markdown';

export default function CurrentLayer(props: {
  value: Layer | null;
  update: (layer: Layer | null) => void;
  setAdditionalLayer: (layer: Layer | null) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  onClose: () => void;
  favorites: boolean;
}) {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [openDialog, setOpenDialog] = useState(false);
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null);
  const [unsubscribeFromLayerMutation, unsubscribeFromLayerMutationResult] = useUnsubscribeFromLayerMutationMutation();
  const [subscribeToLayerMutation, subscribeToLayerMutationResult] = useSubscribeToLayerMutationMutation();

  useEffect(() => {
    if (props.value) {
      showSnackbar(i18n.t('layerSelected', { name: props.value.name }), 'info');
    } else if (props.favorites) {
      showSnackbar(i18n.t('layerSelected', { name: i18n.t('favorites') }), 'info');
    }
  }, [props.value, props.favorites]);

  const goToNewMarker = () => {
    router.push({ pathname: '/new-marker', query: { layerId: props.value?.id } });
    setAnchorMenu(null);
  };

  const goToSettings = () => {
    router.push('/your-layers/[id]', `/your-layers/${props.value?.id}`);
    setAnchorMenu(null);
  };

  const toggleLayerSubscription = (id?: string) => {
    const refetchQueries = ['LayerQuery', 'LayerSubscriptionsQuery'];
    if (id) {
      unsubscribeFromLayerMutation({ variables: { id }, refetchQueries })
        .then((result) => {
          const unsubscribeFromLayer = result.data?.unsubscribeFromLayer;
          if (unsubscribeFromLayer?.success) {
            props.setAdditionalLayer(props.value);
            showSnackbar(unsubscribeFromLayer.message || '', 'success');
          } else {
            showSnackbar(unsubscribeFromLayer?.message || '', 'error');
          }
        })
        .catch(() => {
          showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
        });
    } else {
      subscribeToLayerMutation({ variables: { layerId: props.value?.id as string }, refetchQueries })
        .then((result) => {
          const subscribeToLayer = result.data?.subscribeToLayer;
          if (subscribeToLayer?.success) {
            showSnackbar(subscribeToLayer.message || '', 'success');
          } else {
            showSnackbar(subscribeToLayer?.message || '', 'error');
          }
        })
        .catch(() => {
          showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
        });
    }
  };

  const headerChipComponent = (showTimeInterval = false, icon: JSX.Element, label: string, onClick: () => void) => {
    return (
      <div
        className="current-layer-chip-container"
        style={{ display: 'block', right: !showTimeInterval ? '16px' : '92px' }}
      >
        <Chip
          className="layer-chip current-layer-chip"
          color="primary"
          icon={icon}
          label={label}
          onClick={onClick}
          onDelete={props.onClose}
        />
      </div>
    );
  };

  const showHeaderChip = () => {
    if (props.favorites) {
      return headerChipComponent(false, <Star style={{ fontSize: '1.8rem' }} />, i18n.t('favorites'), () => undefined);
    }
    if (props.value) {
      return headerChipComponent(
        props.value.timeInterval !== 'disabled',
        <Layers style={{ fontSize: '1.8rem' }} />,
        props.value?.name as string,
        () => setOpenDialog(true),
      );
    }
    return <></>;
  };

  return (
    <>
      {showHeaderChip()}

      <Dialog onClose={() => setOpenDialog(false)} open={openDialog} fullWidth>
        <LayerQueryComponent variables={{ id: props.value?.id as string }} skip={!openDialog}>
          {(result) => {
            if (result.data?.layer) {
              props.update(result.data.layer);
            }
            return (
              <DialogContent>
                <div
                  hidden={
                    !session.exists() ||
                    session.getUserId() !== props.value?.user?.id ||
                    props?.value?.suggestions === 'deny'
                  }
                  style={{ position: 'absolute', right: '24px' }}
                >
                  <IconButton onClick={(event) => setAnchorMenu(event.currentTarget)}>
                    <MoreVert />
                  </IconButton>
                  <Menu anchorEl={anchorMenu} open={Boolean(anchorMenu)} onClose={() => setAnchorMenu(null)}>
                    <MenuItem
                      onClick={goToNewMarker}
                      style={
                        session.exists() &&
                        (session.getUserId() === props.value?.user?.id || props.value?.suggestions !== 'deny')
                          ? undefined
                          : { display: 'none' }
                      }
                    >
                      {session.exists() && session.getUserId() === props.value?.user?.id
                        ? i18n.t('newMarker')
                        : i18n.t('suggestNewMarker')}
                    </MenuItem>
                    <MenuItem
                      onClick={goToSettings}
                      style={
                        session.exists() && session.getUserId() === props.value?.user?.id
                          ? undefined
                          : { display: 'none' }
                      }
                    >
                      {i18n.t('settings')}
                    </MenuItem>
                  </Menu>
                </div>
                <div className="icon-centered-container">
                  <Layers className="current-layer-dialog-icon" />
                </div>
                <Typography component="h3" variant="h3" className="dialog-title">
                  {props.value?.name}
                </Typography>
                <Markdown className="dialog-description">{props.value?.description || ''}</Markdown>
                <br />
                <div hidden={!session.exists()}>
                  <Button
                    variant="outlined"
                    fullWidth
                    onClick={() => toggleLayerSubscription(props.value?.currentUserSubscription?.id)}
                    disabled={subscribeToLayerMutationResult.loading || unsubscribeFromLayerMutationResult.loading}
                  >
                    {props.value?.currentUserSubscription?.id ? i18n.t('unsubscribe') : i18n.t('subscribe')} (
                    {props.value?.subscriptionsCount})
                  </Button>
                  <br />
                  <br />
                </div>
              </DialogContent>
            );
          }}
        </LayerQueryComponent>
      </Dialog>
    </>
  );
}
