import { LocationOn } from '@material-ui/icons';
import { Typography } from '@material-ui/core';
import { LocateMarkersQueryComponent, Marker as LayerMarker } from '../../graphql/queries/LocateMarkersQuery.graphql';
import { MarkerClusterGroup, ClusterCustomIcon, Marker, MaterialIcon, Tooltip } from '../react-leaflet';

export default function LocateMarkers(props: {
  variables: { layerId?: string; dateAndTime: Date | null; latitude: number; longitude: number; area: number };
  skip?: boolean;
  currentMarker?: LayerMarker | null;
  onClickMarker: (marker: LayerMarker) => void;
}) {
  return (
    <LocateMarkersQueryComponent variables={props.variables} skip={props.skip}>
      {(result) => (
        <MarkerClusterGroup showCoverageOnHover={false} iconCreateFunction={ClusterCustomIcon} maxClusterRadius={42}>
          {result.data?.locateMarkers?.nodes?.map((marker: LayerMarker) => {
            if (props.currentMarker?.id === marker.id || marker.layer?.id !== props.variables.layerId) {
              return undefined;
            }
            return (
              <Marker
                key={marker?.id}
                position={[marker?.geocodingPoint?.latitude as number, marker?.geocodingPoint?.longitude as number]}
                icon={MaterialIcon(<LocationOn className="leaflet-material-icon-el" fontSize="large" />)}
                onClick={() => props.onClickMarker(marker)}
                riseOnHover
              >
                <Tooltip className="marker-tooltip" offset={[0, -20]} permanent direction="top" opacity={0.8}>
                  <Typography className="marker-tooltip-title">{marker?.name}</Typography>
                </Tooltip>
              </Marker>
            );
          })}
        </MarkerClusterGroup>
      )}
    </LocateMarkersQueryComponent>
  );
}
