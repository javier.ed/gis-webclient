import { LocationOn } from '@material-ui/icons';
import { Typography } from '@material-ui/core';
import {
  LocateFavoriteMarkersQueryComponent,
  FavoriteMarker,
  Marker as LayerMarker,
} from '../../graphql/queries/LocateFavoriteMarkersQuery.graphql';
import { MarkerClusterGroup, Marker, MaterialIcon, Tooltip, ClusterCustomIcon } from '../react-leaflet';

export default function LocateFavoriteMarkers(props: {
  variables: { latitude: number; longitude: number; area: number };
  skip?: boolean;
  currentMarker?: LayerMarker | null;
  onClickMarker: (marker: LayerMarker) => void;
}) {
  return (
    <LocateFavoriteMarkersQueryComponent variables={props.variables} skip={props.skip}>
      {(result) => (
        <MarkerClusterGroup showCoverageOnHover={false} iconCreateFunction={ClusterCustomIcon} maxClusterRadius={42}>
          {result.data?.locateFavoriteMarkers?.nodes?.map((favoriteMarker: FavoriteMarker) => {
            if (props.currentMarker?.id === favoriteMarker.marker?.id) {
              return undefined;
            }
            return (
              <Marker
                key={favoriteMarker?.id}
                position={[
                  favoriteMarker.marker?.geocodingPoint?.latitude as number,
                  favoriteMarker.marker?.geocodingPoint?.longitude as number,
                ]}
                icon={MaterialIcon(<LocationOn className="leaflet-material-icon-el" fontSize="large" />)}
                onClick={() => props.onClickMarker(favoriteMarker.marker as LayerMarker)}
                riseOnHover
              >
                <Tooltip className="marker-tooltip" offset={[0, -20]} permanent direction="top" opacity={0.8}>
                  <Typography className="marker-tooltip-title">{favoriteMarker.marker?.name}</Typography>
                </Tooltip>
              </Marker>
            );
          })}
        </MarkerClusterGroup>
      )}
    </LocateFavoriteMarkersQueryComponent>
  );
}
