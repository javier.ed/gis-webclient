import { useContext } from 'react';
import { Done, Star, MoreHoriz } from '@material-ui/icons';
import { Chip, Avatar } from '@material-ui/core';
import { useRouter } from 'next/router';
import { LayerSubscriptionsQueryComponent, Layer } from '../../graphql/queries/LayerSubscriptionsQuery.graphql';
import { AppContext } from '../../pages/_app';

export default function LayersSubscriptions(props: {
  locateFavorites: boolean;
  onClickFavorites: () => void;
  additionalLayer: Layer | null;
  currentLayer: Layer | null;
  onClickLayer: (layer: Layer | null) => void;
}) {
  const { i18n, session } = useContext(AppContext);
  const router = useRouter();

  if (typeof window !== 'undefined') {
    const goToLayers = () => {
      router.push('/layers', '/layers', { shallow: true });
    };

    return (
      <ul className="layers-chips">
        <li style={session.exists() ? undefined : { display: 'none' }}>
          <Chip
            className="layer-chip"
            color="primary"
            avatar={
              props.locateFavorites ? (
                <Avatar>
                  <Done color="primary" fontSize="small" />
                </Avatar>
              ) : undefined
            }
            icon={props.locateFavorites ? undefined : <Star />}
            label={i18n.t('favorites')}
            onClick={props.onClickFavorites}
          />
        </li>
        <li style={props.additionalLayer ? undefined : { display: 'none' }}>
          <Chip
            className="layer-chip"
            color="primary"
            label={props.additionalLayer?.name}
            onClick={() =>
              props.onClickLayer(
                props.additionalLayer && props.currentLayer?.id === props.additionalLayer.id
                  ? null
                  : props.additionalLayer,
              )
            }
            avatar={
              props.additionalLayer && props.currentLayer?.id === props.additionalLayer.id ? (
                <Avatar>
                  <Done color="primary" fontSize="small" />
                </Avatar>
              ) : undefined
            }
          />
        </li>
        <LayerSubscriptionsQueryComponent>
          {(result) => (
            <>
              {result.data?.layerSubscriptions?.nodes?.map((layerSubscription) => {
                const layer = layerSubscription?.layer;
                if (layer) {
                  return (
                    <li key={layer.id} style={props.additionalLayer?.id === layer.id ? { display: 'none' } : undefined}>
                      <Chip
                        className="layer-chip"
                        color="primary"
                        label={layer.name}
                        onClick={() => props.onClickLayer(props.currentLayer?.id === layer.id ? null : layer)}
                        avatar={
                          props.currentLayer?.id === layer.id ? (
                            <Avatar>
                              <Done color="primary" fontSize="small" />
                            </Avatar>
                          ) : undefined
                        }
                      />
                    </li>
                  );
                }
                return <></>;
              })}
            </>
          )}
        </LayerSubscriptionsQueryComponent>
        <li>
          <Chip
            className="layer-chip"
            color="primary"
            icon={<MoreHoriz />}
            label={i18n.t('moreLayers')}
            onClick={goToLayers}
          />
        </li>
      </ul>
    );
  }
  return <></>;
}
