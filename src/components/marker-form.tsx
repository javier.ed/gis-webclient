import { useContext, useReducer } from 'react';
import { Formik, Form, Field, FormikValues, FormikErrors, FieldArray } from 'formik';
import { TextField, Select, Switch } from 'formik-material-ui';
import {
  FormControl,
  InputLabel,
  MenuItem,
  FormControlLabel,
  Button,
  TextField as MuiTextField,
  InputAdornment,
  IconButton,
  Typography,
} from '@material-ui/core';
import { KeyboardTimePicker, KeyboardDatePicker, KeyboardDateTimePicker } from 'formik-material-ui-pickers';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { Alarm, Event } from '@material-ui/icons';
import { ToggleButtonGroup } from 'formik-material-ui-lab';
import ToggleButton from '@material-ui/lab/ToggleButton';
import { AppContext } from '../pages/_app';
import LocationTextField from './location-text-field';
import ImageField, { allowedImageTypes } from './image-field';

interface ValueProps {
  layerFieldId: string;
  stringValue?: string;
  textValue?: string;
  integerValue?: number;
  floatValue?: number;
  booleanValue?: boolean;
  dateValue?: any;
  timeValue?: any;
  datetimeValue?: any;
  layerFieldOptionId?: string;
}

interface MarkerProps {
  locale: any;
  name: string;
  description: string;
  avatar?: File;
  avatarUrl?: string;
  removeAvatar?: boolean;
  geocodingPoint?: { latitude?: number | null; longitude?: number | null; address?: string | null } | null;
  startAt?: any;
  finishAt?: any;
  values?: ValueProps[] | null;
  translate?: boolean;
}

interface LayerProps {
  id: string;
  name?: string | null;
  timeInterval?: string | null;
  startAtCurrentTime?: boolean | null;
  suggestions?: string | null;
  fields?: {
    id?: string;
    fieldType?: string | null;
    name?: string | null;
    description?: string | null;
    required?: boolean | null;
    unique?: boolean | null;
    editable?: boolean | null;
    options?: {
      id?: string;
      name?: string | null;
    }[];
  }[];
}

export default function MarkerForm(props: {
  initialValues: MarkerProps;
  layer: LayerProps;
  editing?: boolean;
  onSubmit: (
    values: MarkerProps,
    helpers: { setSubmitting: (isSubmitting: boolean) => void; setErrors: (errors: FormikErrors<MarkerProps>) => void },
  ) => void;
  onLocaleChange?: (value: string) => void;
}) {
  const { i18n } = useContext(AppContext);
  const [, forceUpdate] = useReducer((x) => x + 1, 0);
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Formik
        initialValues={props.initialValues}
        validate={(values) => {
          const errors: Partial<FormikValues> = {};
          if (!values.geocodingPoint?.latitude || !values.geocodingPoint?.longitude) {
            errors.geocodingPoint = i18n.t('cantBeBlank');
          }
          if (values.name.trim().length === 0) {
            errors.name = i18n.t('cantBeBlank');
          }
          if (values.avatar && !allowedImageTypes.includes(values.avatar.type)) {
            errors.avatar = i18n.t('isInvalid');
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting, setErrors }) => {
          props.onSubmit(values, { setSubmitting, setErrors });
        }}
      >
        {({ submitForm, isSubmitting, values, errors }) => (
          <Form autoComplete="off">
            <Field
              component={ToggleButtonGroup}
              name="locale"
              type="checkbox"
              exclusive
              onChange={(e: any) => {
                // eslint-disable-next-line no-param-reassign
                values.locale = e.currentTarget.value;
                if (props.onLocaleChange) {
                  props.onLocaleChange(e.currentTarget.value);
                }
                forceUpdate();
              }}
              style={{ float: 'right' }}
            >
              <ToggleButton value="en" aria-label="english">
                {i18n.t('languages.english')}
              </ToggleButton>
              <ToggleButton value="es" aria-label="spanish">
                {i18n.t('languages.spanish')}
              </ToggleButton>
            </Field>
            <MuiTextField
              variant="outlined"
              margin="normal"
              fullWidth
              label={i18n.t('layer')}
              value={props.layer.name}
              disabled
              InputLabelProps={{ shrink: true }}
            />
            <LocationTextField
              initialValues={{ geocodingPoint: values.geocodingPoint }}
              onChange={(geocodingPoint) => {
                // eslint-disable-next-line no-param-reassign
                values.geocodingPoint = { latitude: geocodingPoint.latitude, longitude: geocodingPoint.longitude };
              }}
              error={errors.geocodingPoint}
              disabled={isSubmitting}
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="name"
              label={i18n.t('name')}
            />
            <Field
              component={TextField}
              variant="outlined"
              margin="normal"
              fullWidth
              name="description"
              label={i18n.t('description')}
              multiline
              rows={2}
              rowsMax={4}
              InputLabelProps={{ shrink: values.description.length > 0 }}
            />
            <ImageField
              label={i18n.t('avatar')}
              imageUrl={values.avatarUrl}
              disabled={isSubmitting}
              error={errors.avatar}
              onChangeImage={(image) => {
                if (image) {
                  /* eslint-disable no-param-reassign */
                  values.avatar = image;
                } else if (values.avatarUrl) {
                  values.avatarUrl = undefined;
                  values.removeAvatar = true;
                } else {
                  values.avatar = undefined;
                  /* eslint-enable no-param-reassign */
                }
                forceUpdate();
              }}
            />
            <div hidden={props.layer.timeInterval === 'disabled'}>
              <Field
                style={props.layer.startAtCurrentTime ? { display: 'none' } : undefined}
                component={KeyboardDateTimePicker}
                name="startAt"
                label={i18n.t('startAt')}
                fullWidth
                inputVariant="outlined"
                margin="normal"
                clearable
                format="yyyy-MM-dd HH:mm"
                ampm={false}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton>
                        <Event />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                minDate="1000-01-01"
                maxDate={values.finishAt || undefined}
              />
              <Field
                style={props.layer.timeInterval !== 'any' ? { display: 'none' } : undefined}
                disablePast={props.layer.startAtCurrentTime}
                component={KeyboardDateTimePicker}
                name="finishAt"
                label={i18n.t('finishAt')}
                fullWidth
                inputVariant="outlined"
                margin="normal"
                clearable
                format="yyyy-MM-dd HH:mm"
                ampm={false}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton>
                        <Event />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                minDate={values.startAt || '1000-01-01'}
              />
            </div>
            <br />
            <Typography
              style={{
                fontWeight: 'bold',
                fontSize: '1.5em',
              }}
              color="textSecondary"
            >
              {i18n.t('values')}
            </Typography>
            <FieldArray name="values">
              {() => (
                <>
                  {values.values?.map((_, index) => {
                    const field = props.layer.fields?.[index];
                    switch (field?.fieldType) {
                      case 'boolean':
                        return (
                          <div key={field?.id}>
                            <FormControlLabel
                              label={field?.name}
                              labelPlacement="start"
                              control={
                                <Field
                                  component={Switch}
                                  name={`values.${index}.booleanValue`}
                                  type="checkbox"
                                  disabled={(props.editing && !field?.editable) || isSubmitting}
                                />
                              }
                            />
                          </div>
                        );
                        break;
                      case 'date':
                        return (
                          <Field
                            key={field?.id}
                            component={KeyboardDatePicker}
                            name={`values.${index}.dateValue`}
                            label={field?.name}
                            fullWidth
                            inputVariant="outlined"
                            margin="normal"
                            clearable
                            format="yyyy-MM-dd"
                            required={field?.required}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <Event />
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            disabled={(props.editing && !field?.editable) || isSubmitting}
                          />
                        );
                        break;
                      case 'time':
                        return (
                          <Field
                            key={field?.id}
                            component={KeyboardTimePicker}
                            name={`values.${index}.timeValue`}
                            label={field?.name}
                            fullWidth
                            inputVariant="outlined"
                            margin="normal"
                            clearable
                            format="HH:mm"
                            ampm={false}
                            required={field?.required}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <Alarm />
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            disabled={(props.editing && !field?.editable) || isSubmitting}
                          />
                        );
                        break;
                      case 'datetime':
                        return (
                          <Field
                            key={field?.id}
                            component={KeyboardDateTimePicker}
                            name={`values.${index}.datetimeValue`}
                            label={field?.name}
                            fullWidth
                            inputVariant="outlined"
                            margin="normal"
                            clearable
                            format="yyyy-MM-dd HH:mm"
                            ampm={false}
                            required={field?.required}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <Event />
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            disabled={(props.editing && !field?.editable) || isSubmitting}
                          />
                        );
                        break;
                      case 'choice':
                        return (
                          <FormControl key={field?.id} variant="outlined" margin="normal" fullWidth>
                            <InputLabel htmlFor={`values-${index}-layerFieldOptionId}`}>{field?.name}</InputLabel>
                            <Field
                              component={Select}
                              variant="outlined"
                              name={`values.${index}.layerFieldOptionId`}
                              inputProps={{
                                id: `values-${index}-layerFieldOptionId}`,
                              }}
                              label={field?.name}
                              required={field?.required}
                              disabled={(props.editing && !field?.editable) || isSubmitting}
                            >
                              <MenuItem value="0">{i18n.t('none')}</MenuItem>
                              {field?.options?.map((option) => (
                                <MenuItem key={option?.id} value={option?.id}>
                                  {option?.name}
                                </MenuItem>
                              ))}
                            </Field>
                          </FormControl>
                        );
                        break;
                      default:
                        return (
                          <Field
                            key={field?.id}
                            component={TextField}
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            name={`values.${index}.${field?.fieldType}Value`}
                            label={field?.name}
                            multiline={field?.fieldType === 'text'}
                            type={field?.fieldType === 'integer' ? 'number' : 'text'}
                            required={field?.required}
                            rows={field?.fieldType === 'text' ? 2 : 1}
                            rowsMax={field?.fieldType === 'text' ? 4 : 1}
                            disabled={(props.editing && !field?.editable) || isSubmitting}
                          />
                        );
                    }
                  })}
                </>
              )}
            </FieldArray>
            <br />
            <div hidden={props.editing}>
              <br />
              <FormControlLabel
                hidden={props.editing}
                label={i18n.t('generateTranslations')}
                labelPlacement="start"
                control={<Field component={Switch} name="translate" type="checkbox" />}
              />
            </div>
            <br />
            <br />
            <Button disabled={isSubmitting} onClick={submitForm} variant="contained" color="default" fullWidth>
              {i18n.t('submit')}
            </Button>
            <br />
            <br />
          </Form>
        )}
      </Formik>
    </MuiPickersUtilsProvider>
  );
}
