import {
  Typography,
  Card,
  CardContent,
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  IconButton,
  FormControlLabel,
  Button,
} from '@material-ui/core';
import { Formik, FormikValues, Form, Field, FieldArray, FormikErrors } from 'formik';
import { useContext, useState, useReducer } from 'react';
import { TextField, Select, Switch } from 'formik-material-ui';
import { Close, Add } from '@material-ui/icons';
import { ToggleButtonGroup } from 'formik-material-ui-lab';
import ToggleButton from '@material-ui/lab/ToggleButton';
import { AppContext } from '../pages/_app';
import Collapsible from './collapsible';

interface ValuesProps {
  locale: any;
  name: string;
  description: string;
  timeInterval: string;
  startAtCurrentTime: boolean;
  suggestions: string;
  fields: {
    id?: string;
    fieldType: string;
    name: string;
    options?: string;
    description: string;
    required: boolean;
    unique: boolean;
    editable: boolean;
    delete?: boolean;
  }[];
  translate?: boolean;
}

export const defaultFieldValues = {
  fieldType: 'string',
  name: '',
  options: '',
  description: '',
  required: false,
  unique: false,
  editable: true,
};

export default function LayerForm(props: {
  initialValues: ValuesProps;
  editing?: boolean;
  onSubmit: (
    values: ValuesProps,
    helpers: { setSubmitting: (isSubmitting: boolean) => void; setErrors: (errors: FormikErrors<ValuesProps>) => void },
  ) => void;
  onLocaleChange?: (value: string) => void;
}) {
  const { i18n } = useContext(AppContext);
  const [fieldsCards, setFieldsCards] = useState<{ hidden: boolean }[]>([]);
  const [, forceUpdate] = useReducer((x) => x + 1, 0);

  const hideFieldCard = (index: number) => {
    let newArray = [];
    if (fieldsCards[index]) {
      newArray = fieldsCards.map((f, i) => ({ hidden: i === index ? true : f.hidden }));
    } else {
      newArray = fieldsCards.slice();
      newArray[index] = { hidden: true };
    }
    setFieldsCards(newArray);
  };

  return (
    <Formik
      initialValues={props.initialValues}
      validate={(values) => {
        const errors: Partial<FormikValues> = {};
        if (values.name.trim().length === 0) {
          errors.name = i18n.t('cantBeBlank');
        }
        if (values.description.trim().length === 0) {
          errors.description = i18n.t('cantBeBlank');
        }
        values.fields.forEach((field, index) => {
          if (field.name.trim().length === 0) {
            if (typeof errors.fields === 'undefined') {
              errors.fields = {};
            }
            errors.fields[index] = { name: i18n.t('cantBeBlank') };
          }
        });
        return errors;
      }}
      onSubmit={(values, { setSubmitting, setErrors }) => {
        props.onSubmit(values, { setSubmitting, setErrors });
      }}
    >
      {({ submitForm, isSubmitting, values }) => (
        <Form autoComplete="off">
          <Field
            component={ToggleButtonGroup}
            name="locale"
            type="checkbox"
            exclusive
            onChange={(e: any) => {
              // eslint-disable-next-line no-param-reassign
              values.locale = e.currentTarget.value;
              if (props.onLocaleChange) {
                props.onLocaleChange(e.currentTarget.value);
              }
              forceUpdate();
            }}
            style={{ float: 'right' }}
          >
            <ToggleButton value="en" aria-label="english">
              {i18n.t('languages.english')}
            </ToggleButton>
            <ToggleButton value="es" aria-label="spanish">
              {i18n.t('languages.spanish')}
            </ToggleButton>
          </Field>
          <Field
            component={TextField}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="name"
            label={i18n.t('name')}
          />
          <Field
            component={TextField}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="description"
            label={i18n.t('description')}
            multiline
          />
          <FormControl variant="outlined" margin="normal" fullWidth>
            <InputLabel htmlFor="time-interval">{i18n.t('timeInterval')}</InputLabel>
            <Field
              component={Select}
              variant="outlined"
              name="timeInterval"
              inputProps={{ id: 'time-interval' }}
              label={i18n.t('timeInterval')}
            >
              <MenuItem value="any">{i18n.t('timeIntervalOptions.allowAny')}</MenuItem>
              <MenuItem value="1 day">{i18n.t('timeIntervalOptions.aDayFromCurrentDate')}</MenuItem>
              <MenuItem value="1 week">{i18n.t('timeIntervalOptions.aWeekFromCurrentDate')}</MenuItem>
              <MenuItem value="1 month">{i18n.t('timeIntervalOptions.aMonthFromCurrentDate')}</MenuItem>
              <MenuItem value="disabled">{i18n.t('timeIntervalOptions.disabled')}</MenuItem>
            </Field>
          </FormControl>
          <FormControl variant="outlined" margin="normal" fullWidth>
            <InputLabel htmlFor="suggestions">{i18n.t('markerSuggestions')}</InputLabel>
            <Field
              component={Select}
              variant="outlined"
              name="suggestions"
              inputProps={{ id: 'suggestions' }}
              label={i18n.t('markerSuggestions')}
            >
              <MenuItem value="requires_approval">{i18n.t('markerSuggestionOptions.requiresApproval')}</MenuItem>
              <MenuItem value="publish_directly">{i18n.t('markerSuggestionOptions.publishDirectly')}</MenuItem>
              <MenuItem value="deny">{i18n.t('markerSuggestionOptions.deny')}</MenuItem>
            </Field>
          </FormControl>
          <br />
          <br />
          <Typography
            style={{
              fontWeight: 'bold',
              fontSize: '1.5em',
            }}
            color="textSecondary"
          >
            {i18n.t('fields')}
          </Typography>
          <br />
          <FieldArray name="fields">
            {(arrayHelpers) => (
              <>
                {values.fields.map((field, index) => (
                  <Card
                    key={field.id || index}
                    className="item-card"
                    style={fieldsCards[index]?.hidden ? { display: 'none' } : undefined}
                  >
                    <CardContent>
                      <Grid container spacing={3} alignItems="center">
                        <Grid item xs>
                          <FormControl variant="outlined" margin="normal" fullWidth>
                            <InputLabel htmlFor={`fields-${index}-fieldType`}>{i18n.t('type')}</InputLabel>
                            <Field
                              component={Select}
                              variant="outlined"
                              name={`fields.${index}.fieldType`}
                              inputProps={{ id: `fields-${index}-fieldType` }}
                              label={i18n.t('type')}
                              required
                              disabled={field.id && props.editing}
                            >
                              <MenuItem value="string">{i18n.t('fieldTypes.string')}</MenuItem>
                              <MenuItem value="text">{i18n.t('fieldTypes.text')}</MenuItem>
                              <MenuItem value="integer">{i18n.t('fieldTypes.integer')}</MenuItem>
                              <MenuItem value="float">{i18n.t('fieldTypes.float')}</MenuItem>
                              <MenuItem value="boolean">{i18n.t('fieldTypes.boolean')}</MenuItem>
                              <MenuItem value="date">{i18n.t('fieldTypes.date')}</MenuItem>
                              <MenuItem value="time">{i18n.t('fieldTypes.time')}</MenuItem>
                              <MenuItem value="datetime">{i18n.t('fieldTypes.datetime')}</MenuItem>
                              <MenuItem value="choice">{i18n.t('fieldTypes.choice')}</MenuItem>
                            </Field>
                          </FormControl>
                          <Field
                            component={TextField}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name={`fields.${index}.name`}
                            label={i18n.t('name')}
                          />
                          <Field
                            component={TextField}
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            name={`fields.${index}.options`}
                            label={i18n.t('options')}
                            helperText={i18n.t('onePerLine')}
                            multiline
                            style={field.fieldType === 'choice' ? undefined : { display: 'none' }}
                            disabled={field.id && props.editing}
                          />
                          <Collapsible>
                            <Field
                              component={TextField}
                              variant="outlined"
                              margin="normal"
                              required
                              fullWidth
                              name={`fields.${index}.description`}
                              label={i18n.t('description')}
                              multiline
                            />
                            <Grid spacing={3} container justify="space-around">
                              <Grid item>
                                <FormControlLabel
                                  label={i18n.t('required')}
                                  labelPlacement="start"
                                  control={
                                    <Field component={Switch} name={`fields.${index}.required`} type="checkbox" />
                                  }
                                />
                              </Grid>
                              <Grid item>
                                <FormControlLabel
                                  label={i18n.t('unique')}
                                  labelPlacement="start"
                                  control={<Field component={Switch} name={`fields.${index}.unique`} type="checkbox" />}
                                />
                              </Grid>
                              <Grid item>
                                <FormControlLabel
                                  label={i18n.t('editable')}
                                  labelPlacement="start"
                                  control={
                                    <Field
                                      component={Switch}
                                      name={`fields.${index}.editable`}
                                      label={i18n.t('editable')}
                                      type="checkbox"
                                    />
                                  }
                                />
                              </Grid>
                            </Grid>
                          </Collapsible>
                        </Grid>
                        <Grid item>
                          <IconButton
                            onClick={() => {
                              if (field.id) {
                                // eslint-disable-next-line no-param-reassign
                                field.delete = true;
                                hideFieldCard(index);
                              } else {
                                arrayHelpers.remove(index);
                                setFieldsCards(fieldsCards.filter((p, i) => i !== index));
                              }
                            }}
                          >
                            <Close />
                          </IconButton>
                        </Grid>
                      </Grid>
                    </CardContent>
                  </Card>
                ))}
                <div style={{ textAlign: 'right' }}>
                  <Button
                    variant="text"
                    disabled={isSubmitting}
                    onClick={() => arrayHelpers.push(defaultFieldValues)}
                    startIcon={<Add />}
                  >
                    {i18n.t('addField')}
                  </Button>
                </div>
              </>
            )}
          </FieldArray>
          <br />
          <div hidden={props.editing}>
            <FormControlLabel
              label={i18n.t('generateTranslations')}
              labelPlacement="start"
              control={<Field component={Switch} name="translate" type="checkbox" />}
            />
          </div>
          <br />
          <Button disabled={isSubmitting} onClick={submitForm} variant="contained" color="default" fullWidth>
            {i18n.t('submit')}
          </Button>
          <br />
          <br />
        </Form>
      )}
    </Formik>
  );
}
