import { TextField, InputAdornment, Avatar, IconButton } from '@material-ui/core';
import { useState } from 'react';
import { Backspace } from '@material-ui/icons';

export const allowedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

export default function ImageField(props: {
  label: string;
  imageUrl?: string | null;
  disabled?: boolean;
  error?: string | null;
  onChangeImage: (image?: File | null) => void;
}) {
  const [selectedImage, setSelectedImage] = useState<File | null | undefined>();

  const onChangeImage = (image?: File | null) => {
    props.onChangeImage(image);
    setSelectedImage(image);
  };

  return (
    <TextField
      variant="outlined"
      margin="normal"
      fullWidth
      label={props.label}
      type={props.imageUrl || selectedImage ? 'text' : 'file'}
      value={props.imageUrl?.split('/')?.slice(-1)[0] || selectedImage?.name}
      InputLabelProps={{ shrink: true }}
      disabled={props.disabled}
      InputProps={{
        readOnly: true,
        inputProps: {
          accept: allowedImageTypes.join(','),
        },
        startAdornment: (
          <InputAdornment position="start" style={props.imageUrl || selectedImage ? undefined : { display: 'none' }}>
            <Avatar src={props.imageUrl || (selectedImage ? URL.createObjectURL(selectedImage) : undefined)} />
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end" style={props.imageUrl || selectedImage ? undefined : { display: 'none' }}>
            <IconButton disabled={props.disabled} onClick={() => onChangeImage()}>
              <Backspace />
            </IconButton>
          </InputAdornment>
        ),
        onChange: (event: any) => onChangeImage(event.currentTarget.files[0]),
      }}
      error={typeof props.error !== 'undefined' && props.error != null}
      helperText={props.error}
    />
  );
}
