import { makeStyles, createStyles, InputBase, fade } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { FocusEvent, ChangeEvent, useContext } from 'react';
import { AppContext } from '../pages/_app';

let timeoutQueryId: any;

export default function SearchMenuItem(props: {
  onQueryChanged?: (query: string) => void;
  onFocus?: (event: FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}) {
  const { i18n } = useContext(AppContext);
  const useStyles = makeStyles((theme) =>
    createStyles({
      search: {
        display: typeof props.onQueryChanged !== 'undefined' && props.onQueryChanged !== null ? 'inherit' : 'none',
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.65),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.85),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(1),
          width: 'auto',
        },
        marginRight: '16px',
      },
      searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      inputRoot: {
        color: 'inherit',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          width: '12ch',
          '&:focus': {
            width: '20ch',
          },
        },
      },
    }),
  );
  const classes = useStyles();

  const onQueryChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    if (props.onQueryChanged) {
      if (typeof timeoutQueryId !== 'undefined') {
        clearTimeout(timeoutQueryId);
      }
      timeoutQueryId = setTimeout(props.onQueryChanged, 500, event.currentTarget.value);
    }
  };

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <Search />
      </div>
      <InputBase
        placeholder={i18n.t('search')}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        onChange={onQueryChanged}
        onFocus={props.onFocus}
      />
    </div>
  );
}
