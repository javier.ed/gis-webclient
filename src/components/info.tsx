import { Typography, Grid } from '@material-ui/core';
import { InfoQueryComponent } from '../graphql/queries/InfoQuery.graphql';

export default function Info() {
  return (
    <InfoQueryComponent>
      {(result) => (
        <Grid
          container
          justify="center"
          spacing={3}
          style={{
            marginTop: '4px',
            marginBottom: '4px',
          }}
        >
          <Grid item>
            <Typography
              style={{
                fontWeight: 'bold',
                fontSize: '1.1rem',
              }}
            >
              Bululú Webclient
            </Typography>
          </Grid>
          <Grid item>
            <a href="https://gitlab.com/bululu/bululu-webclient" target="_blank" rel="noreferrer">
              <Typography>Version 0.3-snapshot</Typography>
            </a>
          </Grid>
          <Grid item>
            <a href="https://gitlab.com/bululu/bululu" target="_blank" rel="noreferrer">
              <Typography>Server version {result.data?.info?.serverVersion}</Typography>
            </a>
          </Grid>
        </Grid>
      )}
    </InfoQueryComponent>
  );
}
