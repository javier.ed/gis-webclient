/* eslint-disable import/no-mutable-exports */
/* eslint-disable react/no-unused-prop-types */
import React, { ReactNode } from 'react';
import ReactDOMServer from 'react-dom/server';
import { LocationOn } from '@material-ui/icons';

interface LatLngProps {
  lat: number;
  lng: number;
}

interface MapProps {
  ref?: any;
  children?: ReactNode;
  center?: [number, number] | LatLngProps;
  zoom?: number;
  minZoom?: number;
  maxZoom?: number;
  worldCopyJump?: boolean;
  maxBounds?: [[number, number], [number, number]];
  style?: React.CSSProperties;
  onLoad?: (event: any) => void;
  onClick?: (event: any) => void;
  onResize?: (event: any) => void;
  onMoveStart?: (event: any) => void;
  onMoveEnd?: (event: any) => void;
  onZoomStart?: (event: any) => void;
  onZoomEnd?: (event: any) => void;
}

interface TileLayerProps {
  url?: string;
  attribution?: string;
  maxZoom?: number;
  maxNativeZoom?: number;
  subdomains?: string;
}

interface MarkerProps {
  ref?: any;
  children?: ReactNode;
  position?: [number, number] | LatLngProps;
  icon?: any;
  onClick?: (event: any) => void;
  riseOnHover?: boolean;
  opacity?: number;
  interactive?: boolean;
}

interface PopupProps {
  className?: string;
  children?: ReactNode;
  closeButton?: boolean;
  onClose?: (event: any) => void;
}

interface MarkerClusterGroupProps {
  children?: ReactNode;
  showCoverageOnHover?: boolean;
  iconCreateFunction?: (cluster: any) => any;
  maxClusterRadius?: number;
}

interface TooltipProps {
  className?: string;
  children?: ReactNode;
  offset?: [number, number];
  opacity?: number;
  permanent?: boolean;
  sticky?: boolean;
  direction?: 'right' | 'left' | 'top' | 'bottom' | 'center' | 'auto';
}

/* eslint-disable @typescript-eslint/no-unused-vars */
let Map = (props: MapProps) => <div />;
let TileLayer = (props: TileLayerProps) => <div />;
export let Marker = (props: MarkerProps) => <div />;
export let Popup = (props: PopupProps) => <div />;
export let Tooltip = (props: TooltipProps) => <div />;
export let MarkerClusterGroup = (props: MarkerClusterGroupProps) => <div />;

export let Leaflet = {
  latLng: (latitude: number, longitude: number) => ({ lat: 0, lng: 0 }),
  divIcon: (args: {
    className?: string;
    html?: any;
    iconSize?: [number, number];
    iconAnchor?: [number, number];
    popupAnchor?: [number, number];
  }) => {},
};
/* eslint-enable @typescript-eslint/no-unused-vars */

export function MaterialIcon(
  icon = <LocationOn className="leaflet-material-icon-el" fontSize="large" />,
  className = 'leaflet-material-icon',
) {
  return Leaflet.divIcon({
    className,
    iconSize: [45, 35],
    iconAnchor: [17, 33],
    popupAnchor: [1, -17],
    html: ReactDOMServer.renderToString(icon),
  });
}

export function ClusterCustomIcon(cluster: any, className = '') {
  return Leaflet.divIcon({
    html: `<div>${cluster.getChildCount()}</div>`,
    className: `leaflet-cluster-custom-icon ${className}`,
    iconSize: [40, 40],
  });
}

if (typeof window !== 'undefined') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires, global-require
  const RL = require('react-leaflet');
  Map = RL.Map;
  TileLayer = RL.TileLayer;
  Marker = RL.Marker;
  Popup = RL.Popup;
  Tooltip = RL.Tooltip;
  /* eslint-disable @typescript-eslint/no-var-requires, global-require */
  MarkerClusterGroup = require('react-leaflet-markercluster').default;
  Leaflet = require('leaflet');
  /* eslint-enable @typescript-eslint/no-var-requires, global-require */
}

export function MapWithTileLayer(props: MapProps) {
  return (
    <Map
      ref={props.ref}
      center={props.center}
      zoom={props.zoom || 13}
      minZoom={props.minZoom || 3}
      maxZoom={props.maxZoom || 20}
      worldCopyJump={typeof props.worldCopyJump !== 'undefined' ? props.worldCopyJump : true}
      maxBounds={
        props.maxBounds || [
          [-90, -720],
          [90, 720],
        ]
      }
      style={props.style}
      onLoad={props.onLoad}
      onClick={props.onClick}
      onResize={props.onResize}
      onMoveStart={props.onMoveStart}
      onMoveEnd={props.onMoveEnd}
      onZoomStart={props.onZoomStart}
      onZoomEnd={props.onZoomEnd}
    >
      <TileLayer
        url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors |
                     &copy; <a href="https://carto.com/attributions">CARTO</a>'
        maxNativeZoom={18}
        maxZoom={props.maxZoom || 20}
        subdomains="abcd"
      />
      {props.children}
    </Map>
  );
}
