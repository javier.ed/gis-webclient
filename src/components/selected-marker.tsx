/* eslint-disable jsx-a11y/alt-text */
import { LocationOn, Event, MoreVert } from '@material-ui/icons';
import {
  Avatar,
  Typography,
  Grid,
  Button,
  Dialog,
  DialogContent,
  IconButton,
  Menu,
  MenuItem,
  FormControl,
  InputLabel,
  Box,
  CircularProgress,
} from '@material-ui/core';
import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import { NodeType } from 'react-markdown';
import { AppContext } from '../pages/_app';
import {
  MarkerValuesQueryComponent,
  Marker as LayerMarker,
  LayerFieldType,
  MarkerValueType,
} from '../graphql/queries/MarkerValuesQuery.graphql';
import { Marker, MaterialIcon, Tooltip } from './react-leaflet';
import Collapsible from './collapsible';
import { useCloneMarkerMutationMutation } from '../graphql/mutations/CloneMarkerMutation.graphql';
import Markdown from './markdown';
import { useAddMarkerToFavoritesMutationMutation } from '../graphql/mutations/AddMarkerToFavoritesMutation.graphql';
// eslint-disable-next-line max-len
import { useRemoveMarkerFromFavoritesMutationMutation } from '../graphql/mutations/RemoveMarkerFromFavoritesMutation.graphql';

const stringValueDisallowedTypes: NodeType[] = [
  'blockquote',
  'code',
  'break',
  'inlineCode',
  'list',
  'listItem',
  'table',
  'tableBody',
  'tableCell',
  'tableHead',
  'tableRow',
];

export default function SelectedMarker(props: {
  value?: LayerMarker | null;
  update: (layer: LayerMarker | null) => void;
  onClick?: () => void;
}) {
  const { showSnackbar, i18n, session } = useContext(AppContext);
  const router = useRouter();
  const [openDialog, setOpenDialog] = useState(false);
  const [anchorMenu, setAnchorMenu] = useState<HTMLElement | null>(null);
  const [cloneMarkerMutation] = useCloneMarkerMutationMutation();
  const [addMarkerToFavoritesMutation, addMarkerToFavoritesMutationResult] = useAddMarkerToFavoritesMutationMutation();
  const [
    removeMarkerFromFavoritesMutation,
    removeMarkerFromFavoritesMutationResult,
  ] = useRemoveMarkerFromFavoritesMutationMutation();

  if (props.value) {
    const booleanToString = (value?: boolean | null) => {
      return value ? i18n.t('yes') : i18n.t('no');
    };

    const showTimeRange = (marker?: LayerMarker | null) => {
      if (marker?.startAt || marker?.finishAt) {
        return (
          <>
            <Grid container spacing={1} alignItems="center">
              <Grid item>
                <Event fontSize="large" style={{ color: '#606060' }} />
              </Grid>
              <Grid item xs>
                <Typography style={marker.startAt ? { color: '#666666', fontSize: '0.9rem' } : { display: 'none' }}>
                  {i18n.t('startAt')} {new Date(marker.startAt).toLocaleString()}
                </Typography>
                <Typography style={marker.finishAt ? { color: '#666666', fontSize: '0.9rem' } : { display: 'none' }}>
                  {i18n.t('finishAt')} {new Date(marker.finishAt).toLocaleString()}
                </Typography>
              </Grid>
            </Grid>
          </>
        );
      }
      return <></>;
    };

    const renderValues = (marker?: LayerMarker | null) => {
      if (marker) {
        return marker.layer?.fields?.nodes?.map((field: LayerFieldType) => {
          const value = marker.values?.nodes?.find((v: MarkerValueType) => v.layerField?.id === field.id);
          if (['string', 'text'].includes(field.fieldType || '')) {
            return (
              <FormControl key={field.id} variant="outlined" margin="normal" fullWidth>
                <InputLabel shrink style={{ background: '#fff', padding: '0 5px' }}>
                  {field?.name}
                </InputLabel>
                <Box
                  border={1}
                  borderColor="rgba(0, 0, 0, 0.23)"
                  borderRadius="borderRadius"
                  style={{ padding: '0 14px', fontSize: '1rem', minHeight: '3.555rem' }}
                >
                  <Markdown disallowedTypes={field?.fieldType === 'string' ? stringValueDisallowedTypes : undefined}>
                    {value?.stringValue || value?.textValue || ''}
                  </Markdown>
                </Box>
              </FormControl>
            );
          }
          return (
            <FormControl key={field?.id} variant="outlined" margin="normal" fullWidth>
              <InputLabel shrink style={{ background: '#fff', padding: '0 5px' }}>
                {field?.name}
              </InputLabel>
              <Box
                border={1}
                borderColor="rgba(0, 0, 0, 0.23)"
                borderRadius="borderRadius"
                style={{ padding: '0 14px', fontSize: '1rem', minHeight: '3.555rem' }}
              >
                <p style={{ minHeight: '1.43rem' }}>
                  {value
                    ? value.stringValue ||
                      value.textValue ||
                      value.integerValue ||
                      value.floatValue ||
                      (field?.fieldType === 'boolean' ? booleanToString(value.booleanValue) : undefined) ||
                      (value.dateValue ? new Date(value.dateValue).toLocaleDateString() : undefined) ||
                      (value.timeValue ? new Date(value.timeValue).toLocaleTimeString() : undefined) ||
                      (value.datetimeValue ? new Date(value.datetimeValue).toLocaleString() : undefined) ||
                      value.layerFieldOption?.name ||
                      ''
                    : ''}
                </p>
              </Box>
            </FormControl>
          );
        });
      }
      return (
        <div style={{ display: 'flex' }}>
          <CircularProgress style={{ margin: '8px auto' }} />
        </div>
      );
    };

    const toggleFavorite = (id?: string) => {
      const refetchQueries = ['MarkerQuery', 'MarkerValuesQuery', 'LocateMarkersQuery', 'LocateFavoriteMarkersQuery'];
      if (id) {
        removeMarkerFromFavoritesMutation({ variables: { id }, refetchQueries })
          .then((result) => {
            const removeMarkerFromFavorites = result.data?.removeMarkerFromFavorites;
            if (removeMarkerFromFavorites?.success) {
              showSnackbar(removeMarkerFromFavorites.message || '', 'success');
            } else {
              showSnackbar(removeMarkerFromFavorites?.message || '', 'error');
            }
          })
          .catch(() => {
            showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
          });
      } else {
        addMarkerToFavoritesMutation({ variables: { markerId: props.value?.id as string }, refetchQueries })
          .then((result) => {
            const addMarkerToFavorites = result.data?.addMarkerToFavorites;
            if (addMarkerToFavorites?.success) {
              showSnackbar(addMarkerToFavorites.message || '', 'success');
            } else {
              showSnackbar(addMarkerToFavorites?.message || '', 'error');
            }
          })
          .catch(() => {
            showSnackbar(i18n.t('failedToExecuteRequest'), 'error');
          });
      }
    };

    const attemptToCloneMarker = () => {
      // eslint-disable-next-line no-alert
      if (confirm(i18n.t('areYouSureYouWantToCloneThisMarker'))) {
        cloneMarkerMutation({ variables: { id: props.value?.id as string } })
          .then((result) => {
            const cloneMarker = result.data?.cloneMarker;

            if (cloneMarker?.success) {
              showSnackbar(cloneMarker.message || '', 'success');
              router.push('/your-markers/[id]/edit', `/your-markers/${cloneMarker.marker?.id}/edit`);
            } else {
              showSnackbar(cloneMarker?.message || '', 'error');
            }
          })
          .catch(() => showSnackbar(i18n.t('failedToExecuteRequest'), 'error'));
      }
    };

    const goToSettings = () => {
      router.push('/your-markers/[id]', `/your-markers/${props.value?.id}`);
      setAnchorMenu(null);
    };

    return (
      <>
        <Marker
          position={[props.value?.geocodingPoint?.latitude as number, props.value?.geocodingPoint?.longitude as number]}
          icon={MaterialIcon(<LocationOn className="leaflet-material-icon-el" fontSize="large" />)}
          onClick={props.onClick}
          riseOnHover
        >
          <Tooltip className="current-marker-tooltip" offset={[0, -20]} opacity={1} permanent direction="top">
            <div className="icon-centered-container">
              <Avatar src={props.value?.avatarUrl || undefined} className="marker-tooltip-avatar">
                {props.value?.name?.[0]?.toUpperCase()}
              </Avatar>
            </div>
            <Typography className="marker-tooltip-title current-marker-tooltip-title">{props.value?.name}</Typography>
            <Markdown className="current-marker-tooltip-description">{props.value?.description || ''}</Markdown>
            <Button
              fullWidth
              onClick={(e) => {
                e.preventDefault();
                setOpenDialog(true);
              }}
              variant="outlined"
              size="small"
            >
              {i18n.t('viewMore')}
            </Button>
          </Tooltip>
        </Marker>
        <Dialog onClose={() => setOpenDialog(false)} open={openDialog} fullWidth>
          <MarkerValuesQueryComponent variables={{ id: props.value?.id as string }} skip={!openDialog}>
            {(result) => {
              if (result.data?.marker) {
                props.update(result.data.marker);
              }
              return (
                <>
                  <div
                    style={{
                      display: 'flex',
                      maxHeight: '300px',
                      alignItems: 'center',
                      overflow: 'hidden',
                      backgroundColor: '#d3d3d3',
                    }}
                  >
                    <img src={props.value?.avatarUrl || undefined} style={{ width: '100%' }} />
                  </div>
                  <DialogContent>
                    <Grid container>
                      <Grid item xs>
                        <Typography component="h3" variant="h3" className="dialog-title">
                          {props.value?.name}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        style={
                          session.exists() &&
                          (session.getUserId() === props.value?.user?.id || props.value?.layer?.suggestions !== 'deny')
                            ? undefined
                            : { display: 'none' }
                        }
                      >
                        <IconButton onClick={(event) => setAnchorMenu(event.currentTarget)}>
                          <MoreVert />
                        </IconButton>
                        <Menu anchorEl={anchorMenu} open={Boolean(anchorMenu)} onClose={() => setAnchorMenu(null)}>
                          <MenuItem
                            onClick={attemptToCloneMarker}
                            style={
                              session.exists() &&
                              (session.getUserId() === props.value?.user?.id ||
                                props.value?.layer?.suggestions !== 'deny')
                                ? undefined
                                : { display: 'none' }
                            }
                          >
                            {i18n.t('clone')}
                          </MenuItem>
                          <MenuItem
                            onClick={goToSettings}
                            style={
                              session.exists() && session.getUserId() === props.value?.user?.id
                                ? undefined
                                : { display: 'none' }
                            }
                          >
                            {i18n.t('settings')}
                          </MenuItem>
                        </Menu>
                      </Grid>
                    </Grid>
                    <Markdown className="dialog-description">
                      {props.value?.description || props.value?.description || ''}
                    </Markdown>
                    <br />
                    <Collapsible>
                      {showTimeRange(props.value || props.value)}
                      <Grid container spacing={1} alignItems="center">
                        <Grid item>
                          <LocationOn fontSize="large" style={{ color: '#606060' }} />
                        </Grid>
                        <Grid item xs>
                          <Typography style={{ color: '#666666', fontSize: '0.9rem' }}>
                            {props.value?.geocodingPoint?.address || props.value?.geocodingPoint?.address}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Collapsible>
                    <br />
                    {renderValues(result.data?.marker)}
                    <div hidden={!session.exists()}>
                      <br />
                      <Button
                        variant="outlined"
                        fullWidth
                        onClick={() => toggleFavorite(props.value?.currentUserFavorite?.id)}
                        disabled={
                          addMarkerToFavoritesMutationResult.loading || removeMarkerFromFavoritesMutationResult.loading
                        }
                      >
                        {props.value?.currentUserFavorite?.id
                          ? i18n.t('removeFromFavorites')
                          : i18n.t('addToFavorites')}
                      </Button>
                    </div>
                    <br />
                    <br />
                  </DialogContent>
                </>
              );
            }}
          </MarkerValuesQueryComponent>
        </Dialog>
      </>
    );
  }
  return <></>;
}
