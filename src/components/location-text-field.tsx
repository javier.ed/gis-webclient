import { TextField, InputAdornment, IconButton, Dialog, DialogContent, DialogActions, Button } from '@material-ui/core';
import { useState, useContext, useEffect } from 'react';
import { LocationOn, Backspace } from '@material-ui/icons';
import { QueryResult } from '@apollo/client';
import { AppContext } from '../pages/_app';
import { MapWithTileLayer, Marker, MaterialIcon } from './react-leaflet';
import {
  ReverseGeocodeQueryComponent,
  ReverseGeocodeQueryQuery,
  Exact,
} from '../graphql/queries/ReverseGeocodeQuery.graphql';

export default function LocationTextField(props: {
  initialValues?: {
    geocodingPoint?: { latitude?: number | null; longitude?: number | null; address?: string | null } | null;
  };
  onChange?: (geoCodingPoint: { latitude?: number; longitude?: number }) => void;
  error?: string;
  disabled?: boolean;
}) {
  const { i18n, getCurrentPosition } = useContext(AppContext);
  const [openDialog, setOpenDialog] = useState(false);
  const [initialCenter, setInitialCenter] = useState<[number, number]>([0, 0]);
  const [selectedLocation, setSelectedLocation] = useState<[number, number] | undefined>();
  const [selectedLocationTextField, setSelectedLocationTextField] = useState('');
  const [locationTextField, setLocationTextField] = useState('');
  const [geocodingPoint, setGeocodingPoint] = useState<{ latitude?: number; longitude?: number }>();

  useEffect(() => {
    const point = props.initialValues?.geocodingPoint;
    if (point?.latitude && point.longitude && point.address) {
      setLocationTextField(`${point.address} (${point.latitude}, ${point.longitude})`);
      setGeocodingPoint({ latitude: point.latitude, longitude: point.longitude });
    }
  }, []);

  const handleOpenDialog = () => {
    if (props.disabled) {
      return;
    }
    setOpenDialog(true);
    if (geocodingPoint?.latitude && geocodingPoint?.longitude) {
      setInitialCenter([geocodingPoint.latitude, geocodingPoint.longitude]);
      setSelectedLocation([geocodingPoint.latitude, geocodingPoint.longitude]);
      setSelectedLocationTextField(locationTextField);
    } else {
      getCurrentPosition((value) => {
        setInitialCenter([value.coords.latitude, value.coords.longitude]);
      });
    }
  };

  const handleMapOnClick = (event: any) => {
    const { lat, lng } = event.latlng;
    setSelectedLocation([lat, lng]);
  };

  const removeSelectedLocation = () => {
    setSelectedLocation(undefined);
    setSelectedLocationTextField('');
  };

  const handleDialogClose = () => {
    setOpenDialog(false);
    removeSelectedLocation();
  };

  const handleDialogOk = () => {
    setLocationTextField('');
    setOpenDialog(false);
    setGeocodingPoint({ latitude: selectedLocation?.[0], longitude: selectedLocation?.[1] });
    setLocationTextField(selectedLocationTextField);
    if (typeof props.onChange !== 'undefined') {
      props.onChange({ latitude: selectedLocation?.[0], longitude: selectedLocation?.[1] });
    }
    removeSelectedLocation();
  };

  const changeSelectedLocationTextField = (
    result: QueryResult<ReverseGeocodeQueryQuery, Exact<{ latitude: number; longitude: number }>>,
  ) => {
    if (selectedLocation) {
      setSelectedLocationTextField(
        `${result.loading ? '' : result.data?.reverseGeocode?.address} (${selectedLocation[0]}, ${
          selectedLocation[1]
        })`,
      );
    } else {
      setSelectedLocationTextField('');
    }
  };

  return (
    <>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        label={i18n.t('location')}
        value={locationTextField}
        error={typeof props.error !== 'undefined'}
        helperText={props.error}
        InputProps={{
          readOnly: true,
          endAdornment: (
            <InputAdornment position="end">
              <IconButton disabled={props.disabled}>
                <LocationOn />
              </IconButton>
            </InputAdornment>
          ),
        }}
        InputLabelProps={{ shrink: locationTextField.length > 0 }}
        onClick={handleOpenDialog}
        disabled={props.disabled}
      />
      <Dialog onClose={handleDialogClose} open={openDialog} fullWidth maxWidth="lg">
        <DialogContent>
          <MapWithTileLayer
            center={initialCenter}
            style={{
              cursor: 'pointer',
              maxHeight: 'calc(100% - 80px)',
              height: 'calc(100vh - 240px)',
            }}
            onClick={handleMapOnClick}
          >
            <Marker
              position={selectedLocation || [0, 0]}
              icon={MaterialIcon(
                <LocationOn className="leaflet-material-icon-el" fontSize="large" />,
                selectedLocation ? 'leaflet-material-icon' : 'leaflet-material-icon-hidden',
              )}
              riseOnHover
            />
          </MapWithTileLayer>
          <ReverseGeocodeQueryComponent
            variables={
              selectedLocation == null
                ? { latitude: 0, longitude: 0 }
                : { latitude: selectedLocation[0], longitude: selectedLocation[1] }
            }
            skip={typeof selectedLocation === 'undefined' || selectedLocation === null}
          >
            {(result) => {
              changeSelectedLocationTextField(result);
              return (
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label={i18n.t('selectedLocation')}
                  value={selectedLocationTextField}
                  InputProps={{
                    readOnly: true,
                    endAdornment: (
                      <InputAdornment
                        position="end"
                        style={selectedLocationTextField.length === 0 ? { display: 'none' } : undefined}
                      >
                        <IconButton onClick={removeSelectedLocation}>
                          <Backspace />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  InputLabelProps={{ shrink: selectedLocationTextField.length > 0 }}
                />
              );
            }}
          </ReverseGeocodeQueryComponent>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose}>{i18n.t('cancel')}</Button>
          <Button onClick={handleDialogOk}>{i18n.t('ok')}</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
