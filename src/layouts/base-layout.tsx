import React, { useContext } from 'react';
import Head from 'next/head';
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Container,
  ThemeProvider,
  CssBaseline,
  Hidden,
} from '@material-ui/core';
import ArrowBackOutlined from '@material-ui/icons/ArrowBackOutlined';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import Link from 'next/link';
import theme from '../utils/theme';
import { AppContext } from '../pages/_app';
import SearchMenuItem from '../components/search-menu-item';
import Info from '../components/info';

interface BaseLayoutProps {
  title?: string | null;
  // eslint-disable-next-line react/no-unused-prop-types
  appBarTitle?: React.ReactNode;
  onBackButtonClick?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  onSearchQueryChanged?: (query: string) => void | null;
  menu?: JSX.Element | null;
  children?: React.ReactNode;
  noindex?: boolean;
  nofollow?: boolean;
}

export default function BaseLayout(props: BaseLayoutProps) {
  const { i18n } = useContext(AppContext);
  const router = useRouter();

  const goBack = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    if (typeof props.onBackButtonClick !== 'undefined' && props.onBackButtonClick != null) {
      props.onBackButtonClick(event);
    } else {
      router.back();
    }
  };

  const pageTitle = () => {
    if (props.title) {
      return `${props.title} | Bululú`;
    }
    return `Bululú - ${i18n.t('appDescription')}`;
  };

  const appBarTitle = () => {
    if (props.appBarTitle) {
      return props.appBarTitle;
    }
    return (
      <Typography className="app-bar-title" color="textPrimary">
        {props.title}
      </Typography>
    );
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Head>
        <title>{pageTitle()}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NextSeo
        title={`${pageTitle()}`}
        openGraph={{
          title: `${pageTitle()}`,
        }}
        noindex={props.noindex}
        nofollow={props.nofollow}
      />
      <AppBar className="app-bar">
        <Toolbar>
          <IconButton href="/" onClick={goBack} className="app-bar-back-button">
            <ArrowBackOutlined fontSize="large" />
          </IconButton>
          <Hidden xsDown>
            <Link href="/" passHref>
              <IconButton className="app-bar-icon-container">
                <img src="/icon.svg" alt="Logo" />
              </IconButton>
            </Link>
          </Hidden>

          {appBarTitle()}

          <div style={{ flexGrow: 1 }} />

          <SearchMenuItem onQueryChanged={props.onSearchQueryChanged} />

          {props.menu}
        </Toolbar>
      </AppBar>

      <Container fixed className="container">
        <div style={{ marginBottom: '12px', minHeight: 'calc(100vh - 150px)' }}>{props.children}</div>
        <Info />
      </Container>
    </ThemeProvider>
  );
}
