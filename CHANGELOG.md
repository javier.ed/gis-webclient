# CHANGELOG


## [0.2.0] - 2020-09-23

### Added

* Added feature to save content with translations !37 !34
* Added features to manage favorite markers !36
* Added event to MenuItem to go to 'New Marker' form !29

### Changed

* Load marker description from search results !33
* Show empty values !30

### Fixed

* Some UI improvements !35
* Disable button 'Add Field' when is submitting !32
* Allow to select dates from 1900-01-01 !31


## [0.1.0] - 2020-07-28

### Added

* Added feature to parse markdown in descriptions and text values !28
* Added feature to change email !27
* Added feature to clone markers !26
* Added layer settings to set time intervals and to allow or disallow suggestions !25
* Added time range fields to marker form, and a datetime picker to home page !23
* Added feature to update profile !21

### Changed

* Dependencies upgraded !24

### Removed

* Removed marker data to directly use values !22


## [0.0.0] - 2020-07-15

### Added

* All is new
* Added GitLab CI with ESLint and Stylelint !20
* Added feature to change password !19
* Show info !18
* Added feature to search markers !17
* Added feature to subscribe or unsubscribe from layers !16
* Added favicon & webmanifest !15
* Added feature to approve or reject markers !14
* Added feature to delete layers !13
* Added feature to delete markers !12
* Added feature to update markers !11
* Added feature to update layers !10
* Added next-seo !9
* Added page transitions !8
* Added feature to verify email address !7
* Added layerId variable to LocateMarkersQuery !6
* Markers are grouped with Leaflet.markercluster !5
* Added an environment variable to disable user registrations !4
* Added support for environment variables !3
* License added !2
* Added feature to create markers !1
